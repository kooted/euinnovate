#!/bin/bash
source $(dirname $0)/env.sh

function realpath {
    python -c "import os, sys; print os.path.abspath(sys.argv[1])" "$1"
}

rm -rf "${BUILD_PATH}/"*

XLWRITE="$BUILD_PATH/+xlwrite"
XLREAD="$BUILD_PATH/+xlread"
IO_PATH="${BUILD_PATH}/io"

for DIR in "$BUILD_PATH" "$XLWRITE" "$XLREAD" "$IO_PATH" "$TEMPLATES"; do
    if [ ! -d "$DIR" ]; then
        mkdir -p "$DIR"
    fi
done

cp -R "${MLAB_ROOT}/+xlwrite/poi_library" $XLWRITE
cp "${MLAB_ROOT}/+xlread/"*.m $XLREAD

# copy IO templates to MATLAB build folder
cp "${MLAB_ROOT}/io_templates/"*.xlsx "$IO_PATH"

# copy IO templates to EU-InnovatE public download folder
cp "${MLAB_ROOT}/io_templates/"*.xlsx "$TEMPLATES"

cd "$BUILD_PATH"
mcc -I "$MLAB_ROOT" -mv -o "euinnovate" standaloneWrapper.m InnovateModel.m
