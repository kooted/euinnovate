#!/bin/bash

# install system package dependensies
sudo apt-get install libpostgresql-jdbc-java python-pip virtualenv libpq-dev python-dev rabbitmq-server libapache2-mod-wsgi apache2 celeryd

source $(dirname $0)/env.sh

# create virtualenv
if [ ! -d "$ENV_PATH" ]; then
    virtualenv "$ENV_PATH"
fi 

# activate virtualenv
source "$ENV_PATH/bin/activate"

pip install --upgrade pip
pip install -r "$PROJECT_ROOT/requirements.txt"

# Install the project in editable mode (setuptools develop)
pip install -e "$PROJECT_ROOT"

# init database
cd "$PROJECT_ROOT"
gearbox setup-app


# ln -s /etc/apache2/sites-available/euinnovate.conf /etc/apache2/sites-enabled/euinnovate.conf

ln -s /etc/apache2/mods-available/wsgi.load /etc/apache2/mods-enabled/wsgi.load
ln -s /etc/apache2/mods-available/wsgi.conf /etc/apache2/mods-enabled/wsgi.conf