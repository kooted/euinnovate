#!/bin/bash

source $(dirname $0)/env.sh

# Activate virtualenv
source "$ENV_PATH/bin/activate"

# Start TurboGears project
cd $PROJECT_ROOT
"$@"
