classdef ExcelPolicyReader < xlread.ExcelReader
    %EXCELINNOVATIONREADER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function self = ExcelPolicyReader(filename, sheet, range)
            % Call superclass constructor
            self@xlread.ExcelReader(filename, sheet, range); 
        end
        
        function rv = getPolicy(self)
%             self.data_(isnan(self.data_)) = [];
            policy = struct(...
                'adoption_rate', self.data_);
            rv = policy;
        end
    end
    
end

