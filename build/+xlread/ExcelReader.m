classdef ExcelReader < handle
    %EXCELCONFIGREADER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename
        sheet
        range
        data_
        properties_
        raw_
        dict
    end
    
    methods
        function self = ExcelReader(filename, sheet, range)
            self.filename = filename;
            self.sheet = sheet;
            self.range = range;
            self.dict = struct();
            self.readExcelFile();
        end
        
        function readExcelFile(self)
            [self.data_, self.properties_, self.raw_] = xlsread(self.filename, self.sheet, self.range);
        end
        
        function parseDict(self)
            for row = self.raw_'
                key = row{1};
                value = row{3};
                if ~isnan(key)
                    if isfield(self.dict, key)
                        % the key already exists in dictionary, then
                        % we have encountered a vector. Append value to the
                        % existing key
                        assert(~ischar(value), 'String arrays are not supported');
                        self.dict.(key) = [self.dict.(key) value];
                    else
                        self.dict.(key) = value;
                    end
                end
            end
        end
    end
end

