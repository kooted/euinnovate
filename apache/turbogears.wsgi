import os, sys
import logging.config


APP_CONFIG = 'production.ini'
ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
SITEDIR = os.path.join(ROOT, '.env/lib/python2.7/site-packages')
PKGDIR = os.path.join(ROOT, 'euinnovate')

if SITEDIR:
    import site
    sys_path = set(sys.path)
    is_sys_path = lambda path: path in sys_path
    site.addsitedir(SITEDIR)
    sys.path.sort(key=is_sys_path)

sys.path.append(ROOT)
sys.path.append(PKGDIR)

# virtualenv is activated, start the app
from paste.deploy import loadapp

logging.config.fileConfig(APP_CONFIG)
application = loadapp('config:{}'.format(os.path.join(ROOT, APP_CONFIG)))

