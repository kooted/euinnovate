/* User interface enhancements
 */
$(function() {
    ///////
    // A default set of reusable plot options.  Note that line
    // smoothing is turned on.  Bands will be drawn with the same 
    // smoothing as the line.
    //////
    myTheme = {
      grid: {
          drawBorder: false,
          shadow: false,
          background: 'rgba(255, 255, 255, 0.0)'
      },
      seriesDefaults: {
        shadow: false,
        showMarker: false
      },
      axes: {
          xaxis: {
              pad: 1.0,
              tickOptions: {
                  showGridline: false
              }
          },
          yaxis: {
              pad: 1.05
          }
      }
    };


    $.each(meow['charts'], function(idx, chart){
        var cust = {
            legend:{
                renderer: $.jqplot.EnhancedLegendRenderer,
                show:true
            },
            series: []
        };
        $.each(meow['labels'], function (j, label) {
            cust['series'].push({
                label: label,
                rendererOptions: {
                    bandData: meow[chart]['ci'][j],
                    smooth: true
                }
            });
        });
        plot1 = $.jqplot('chart_'+chart, meow[chart]['mean'], $.extend(true, {}, myTheme, cust));
    });
})