classdef ExcelInnovationReader < xlread.ExcelReader
    %EXCELINNOVATIONREADER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function self = ExcelInnovationReader(filename, sheet, range)
            % Call superclass constructor
            self@xlread.ExcelReader(filename, sheet, range); 
            self.parseDict();
        end
        
        function rv = getInnovation(self)
            % Populate 'innovation' structure from Excel spreadsheet
            innovation = struct(...
                'domain', self.dict.domain, ...
                'primary_impact', self.dict.primary_impact, ...
                'impacts', '');

            % Assign impacts and probability of adoption depending on
            % innovation domain
            if strcmpi( innovation.domain, 'living' )
                if strcmpi( innovation.primary_impact, 'change type' )
                    innovation.impacts = [ self.dict.living_change_type ];
                elseif strcmpi( innovation.primary_impact, 'improve construction' )
                    innovation.impacts = [ self.dict.living_improve_construction ];
                elseif strcmpi( innovation.primary_impact, 'reduce household waste' )
                    innovation.impacts = [ self.dict.living_reduce_waste ];
                elseif strcmpi( innovation.primary_impact, 'reduce space' )
                    innovation.impacts = [ self.dict.living_reduce_space ];
                else
                    error('Invalid Primary Impact');
                end
            elseif strcmpi( innovation.domain, 'food' )
                if strcmpi( innovation.primary_impact, 'reduce consumption' )
                    innovation.impacts = [ self.dict.food_reduce_consumption ];
                elseif strcmpi( innovation.primary_impact, 'reduce waste' )
                    innovation.impacts = [ self.dict.food_reduce_waste ];
                elseif strcmpi( innovation.primary_impact, 'preparation' )
                    innovation.impacts = [ self.dict.food_preparation ];
                elseif strcmpi( innovation.primary_impact, 'change source' )
                    innovation.impacts = [ self.dict.food_change_source ];
                else
                    error('Invalid Primary Impact');
                end
            elseif strcmpi( innovation.domain, 'mobility' )
                if strcmpi( innovation.primary_impact, 'reduce km' )
                    innovation.impacts = [ self.dict.mobility_reduce_km ];
                elseif strcmpi( innovation.primary_impact, 'change fuel' ) % adopt electric car
                    innovation.impacts = [ ];
                elseif strcmpi( innovation.primary_impact, 'increase efficiency' )
                    innovation.impacts = [ self.dict.mobility_increase_efficiency ];
                elseif strcmpi( innovation.primary_impact, 'change mode' )
                    innovation.impacts = [ self.dict.mobility_change_mode ];
                else
                    error('Invalid Primary Impact');
                end
            elseif strcmpi( innovation.domain, 'energy' )
                if strcmpi( innovation.primary_impact, 'reduce consumption' )
                    innovation.impacts = [ self.dict.energy_reduce_consumption ];
                elseif strcmpi( innovation.primary_impact, 'change source' )
                    innovation.impacts = [ self.dict.energy_change_source ];
                else
                    error('Invalid Primary Impact');
                end
            else
                error('Invalid innovation.domain - Exit');
            end
            rv = innovation;
        end
    end
    
end

