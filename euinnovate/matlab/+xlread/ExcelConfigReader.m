classdef ExcelConfigReader < xlread.ExcelReader
    %EXCELCONFIGREADER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function self = ExcelConfigReader(filename, sheet, range)
            % Call superclass constructor
            self@xlread.ExcelReader(filename, sheet, range); 
            self.data_(isnan(self.data_)) = []; % remove empty spaces from model_data vector
            self.parseDict();
        end
        
        function config = getConfig(self)  
            config = self.dict;
            config.floor_area_range = [20 50 70 90 110 300];
            config.pv_size = [ 1 2 3 4 ]; % kW of system size
        end   
    end
end

