classdef Population < handle
    %POPULATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % all fields in CAPITALS are one per population
        config;
        numberOfAgents;
        id;
        adopter = [];
        status = [];
        living = struct(...
            'occupancy', [], ...
            'location', [], ...
            'space', -1, ...
            'construction', -1, ...             % energy efficiency factor [0.5 .. 1.5]. Used to scale heating energy consumption
            'waste', struct(...
                'recyclable', -1, ...
                'nonrecyclable', -1), ...
            'type', -1, ...
            'floor_area', -1);
        food = struct(...
            'food_consumed', -1, ...
            'food_waste', -1, ...
            'preparation', 1);
        energy = struct(...
            'energy_source', -1, ...
            'house_size_scaling', 0, ...        % {0.5, 1, 2} scaling of avg_heat_energy_demand for houses of different size
            'annual_base_heat_loss_pu', 0, ...          % amount of energy lost per year due to transmission per unit area
            'domestic_emissions', 0, ...
            'pv_present', -1, ...               % Installed photovoltaics capacity in kW; size(1 x numberOfAgents) 
            'energy_reduction_factors', [1, 1, 1, 1, 1], ...
            'grid_reduction_factor', 1, ...
            'local_energy_source_present', [0, 0, 0, 0], ...
            'energy_usage', 0);
        mobility = struct(...
            'MAX_CARS', -1, ...
            'cars', -1, ...
            'fuel', -1, ...
            'emissions', 0, ...
            'efficiency', -1, ...
            'fuel_consumption', zeros(1, 3), ...
            'km_travelled', zeros(1,5)); % [walking, cycling, car, bus, train]'
    end
    
    methods
        function self = Population(config, numberOfAgents)
            self.config = config;
            self.numberOfAgents = numberOfAgents;
            
            self.adopter = zeros(1, numberOfAgents);
            % each household has a probability of "start_inert" of
            % being inert, where 0=inert, 1=active
            self.status = v_randsample(numberOfAgents, ...
                [config.start_inert / 100, 1 - config.start_inert / 100]) - 1;

            %% LIVING Profiles
            % assign household location Rural=0, Urban=1
            prob_urban = config.percent_urban / 100;
            self.living.location = v_randsample(numberOfAgents, ...
                [1 - prob_urban, prob_urban]) - 1;
            % assign household occupancy from {1, 2, 3, 4, 5, 6}
            self.living.occupancy = v_randsample(numberOfAgents, config.dist_residents);
            % assign household floor space from {1, 2, 3, 4, 5}
            self.living.space = v_randsample(numberOfAgents, config.dist_floor_area);
            % floor area is calculated by taking the 'floor_area_range'
            % bins from config and adding a random percentage of deltas in
            % between successive bins
            % deltas between successive floor area bins
            c = config.floor_area_range;
            delta_space = c(2:end) - c(1: end-1);
            % floor area is a matrix 'space bins' x 'number of agents'
            floor_area = delta_space' * rand(1, numberOfAgents) + ...
                repmat(c(1:end-1)', 1, numberOfAgents);
            % Perform selection from floor_area according to
            % self.living.space
            indices = sub2ind(size(floor_area), self.living.space, 1:length(self.living.space));
            self.living.floor_area = floor_area(indices);
            % assign household construction
            pd = makedist('Triangular', ... 
                'a', config.lower_construction_bound, ...
                'b', config.peak_construction_bound, ...
                'c', config.upper_construction_bound);
            self.living.construction = random(pd, 1, numberOfAgents);
            % assign household type [private=0 OR co-op=1]
            prob_coop = config.percent_coop / 100;
            self.living.type = v_randsample(numberOfAgents, [1 - prob_coop, prob_coop]) - 1;
            % assign household waste [recycled, nonrecycled]
            total_waste = self.living.occupancy * config.waste_per_person;
            self.living.waste.recyclable = total_waste * config.percent_waste_recycled / 100;
            self.living.waste.nonrecyclable = total_waste * ...
                (100 - config.percent_waste_recycled) / 100;
    
            %% FOOD Profiles
            % assign household food consumption PER YEAR
            self.food.food_consumed = config.household_food * self.living.occupancy;
            self.food.food_waste = [config.percent_food_waste / 100 * ...
                self.food.food_consumed * config.avoidable_waste_percent / 100; ...
                config.percent_food_waste / 100 * self.food.food_consumed * ...
                (1 - (config.avoidable_waste_percent / 100 ))];
            self.food.preparation = ones(1, numberOfAgents); % cooking energy reduction factor
            
            %%  ENERGY Profiles
            % assign heating source from [electric, gas, oil, solid]
            % electric = 0, gas = 1, oil = 2, solid = 3
            self.energy.energy_source = v_randsample(numberOfAgents, config.heating_source) - 1;
            % assign PV ownership and calculate output
            % [0, 1] = [no pv, pv present]
            has_pv = v_randsample(numberOfAgents, [1 - config.houses_with_pv, config.houses_with_pv]) - 1;
            % randomly assign PV size from [1kW, 2kW, 3kW, 4kW]
            weight = ones(1, length(config.pv_size)) / length(config.pv_size);
            % assign PV capacity to those that have it.
            self.energy.pv_present = has_pv .* v_randsample(numberOfAgents, weight);
            
            
            % value assigned when consumption is calculated
            self.energy.energy_usage = zeros(1, numberOfAgents);
            % value assigned when consumption is calculated
            self.energy.domestic_emissions = zeros(1, numberOfAgents);
            % [heating_reduction_factor, hot_water_reduction_factor, ...
            % lights_reduction_factor, appliance_reduction_factor, ...
            % cooking_reduction_factor ]
            self.energy.energy_reduction_factors = ones(5, numberOfAgents);
            self.energy.grid_reduction_factor = ones(1, numberOfAgents);
            self.energy.local_energy_source_present = zeros(4, numberOfAgents);
            
            %% MOBILITY Profiles
            % assign car ownership from [0, 1, 2, 3, 4]
            self.mobility.cars =  v_randsample(numberOfAgents, config.num_cars) - 1;
            
            self.mobility.MAX_CARS = length(config.num_cars) - 1;
            car_bins = 0:self.mobility.MAX_CARS;
            cars_indices = zeros(length(car_bins), numberOfAgents);
            for i = car_bins
               C = self.mobility.cars - i;
               C(C < 0) = 0;
               C = C + 1;
               cars_indices(sub2ind(size(cars_indices), C, 1:length(C))) = 1;
            end
            cars_indices(1,:) = [];

            % assign fuel to cars
            % car only - petrol=1/diesel=2/electric=3
            self.mobility.fuel = [...
                v_randsample(numberOfAgents, config.prob_fuel);
                v_randsample(numberOfAgents, config.prob_fuel);
                v_randsample(numberOfAgents, config.prob_fuel);
                v_randsample(numberOfAgents, config.prob_fuel)] .* cars_indices;
            
            % assign household km travelled PER YEAR (spilt Rural/Urban)
            % Multiplication factors for rural r(1, :) and urban r(2, :)
            % locations
            r = [(1 + ((50 - 30) * rand(1, numberOfAgents) + 30) / 100); ...
                 (1 - ((15 - 5) * rand(1, numberOfAgents) + 5) / 100)];
            self.mobility.km_travelled = config.mean_dist_travel' * ...
                self.living.occupancy;
            
            % Select from R based on self.living.location+1
            indices = sub2ind(size(r), self.living.location+1, 1:length(self.living.location));            
            self.mobility.km_travelled = self.mobility.km_travelled .* ...
                repmat(r(indices), size(self.mobility.km_travelled, 1), 1);
            
            km_travelled_by_car = self.mobility.km_travelled(3, :);
            self.mobility.km_travelled(4,:) = self.mobility.km_travelled(4,:) + ...
                round(km_travelled_by_car / 2) .* (self.mobility.cars == 0);
            self.mobility.km_travelled(5,:) = self.mobility.km_travelled(4,:) + ...
                round(km_travelled_by_car / 2) .* (self.mobility.cars == 0);
            % if households owns zero cars, redistribute miles between
            % bus/train
            km_travelled_by_car(self.mobility.cars == 0) = 0;
            self.mobility.km_travelled(3, :) = km_travelled_by_car;
            
            % calculate fuel consumption for cars...assume km travelled by 
            % car is equally distributed between all household cars
            % consumption of [petrol, diesel, electricity] - value assigned 
            % when consumption is calculated
            self.mobility.fuel_consumption = zeros(3, numberOfAgents);
            
            % calculate CO2 emissions for car, bus, train
            % value assigned when consumption is calculated
            self.mobility.emissions = zeros(1, numberOfAgents); 
            
            % efficiency is currently a dummy parameter that will be used to
            % incorporate innovations with increased fuel efficiency and/or
            % reduced CO2 emissions- NOT USED
            self.mobility.efficiency = ones(self.mobility.MAX_CARS, numberOfAgents) .* logical(self.mobility.fuel);
              
%             self.updateConsumption();

            %% INITIAL ENERGY USAGE
            % heating energy
            heating_energy = self.getHouseSizeScaling() .* ...
                             self.config.heating_demand .* ...
                             self.energy.energy_reduction_factors(1,:);
            non_heating_energy = self.getNonHeatingEnergy();
            
            self.energy.annual_base_heat_loss_pu = (heating_energy + ...
            	non_heating_energy.contribution_to_heating) ./ self.getNormalisedBuildingSurfaceArea();
        end
        
        function kg = getKgConsumption(self)
            % Annual consumption in kilograms
            kg = (self.energy.energy_usage - self.getAnnualPVOutput()) * ...
                sum(self.config.proportion_electricity .* ...
                self.config.kgs_per_kWh_electricity) + ...
                self.mobility.fuel_consumption(2,:) * 1.5 * 100 / 6.0 + ... % diesel
                self.mobility.fuel_consumption(1,:) * 1.5 * 100 / 6.0 + ... % petrol
                self.living.waste.nonrecyclable + ...
                self.food.food_consumed * 6000 / 550 + ...
                self.food.food_waste(1,:) * 6000 / 550;
        end
        
        function rv = getNormalisedBuildingSurfaceArea(self)
            % get normalised surface area for heat loss calculations from
            % floor area
            length = sqrt(self.living.floor_area) * 1.2;
            width = sqrt(self.living.floor_area) / 1.2;
            height = 2.5;
            rv = 2 * height .* (length + width) + 1.65 * length .* width;
        end
        
        function rv = getNonHeatingEnergy(self)
            % Calculate household non-heating energy needs
            % electric vehicle charging
            % FIXME: electric vehicle charging should not be equal to hybrid 
            % vehicle energy spend, as it is now
            rv = struct(...
                'hot_water', self.config.hot_water_demand_pp * ...
                             self.living.occupancy .* ...
                             self.energy.energy_reduction_factors(2,:), ...
                'lightning', self.config.lights_demand * ...
                             self.energy.energy_reduction_factors(3,:), ...
                'appliances', self.config.appliance_demand_pp * ...
                              self.living.occupancy .* ...
                              self.energy.energy_reduction_factors(4,:), ...
                'cooking', self.config.cooking_demand * ...
                           self.food.preparation .* ...
                           self.energy.energy_reduction_factors(5,:), ...
                'car_charging', self.mobility.fuel_consumption(3,:), ...
                'contribution_to_heating', [] ...
            );
            rv.contribution_to_heating = (0.3 * rv.hot_water + 0.8 * rv.lightning + ...
                rv.appliances + 0.8 * rv.cooking + self.getHumanHeat()) * 0.7;
        end

        
        function house_size_scaling = getHouseSizeScaling(self)
            house_size_scaling = (self.living.space == 1) / 2.0 + ...
                (self.living.space >= 2 & self.living.space <= 4) + ...
                (self.living.space > 4) * 2;
        end
        
        function human_body_heat = getHumanHeat(self)
            % heat produced by human bodies (100W/person * 12h/day * 340 days)
            human_body_heat = self.living.occupancy * 0.1 * 12 * 340;
        end
        
        function pv_output = getAnnualPVOutput(self)
            % Annual PV output calculation in kWh
            pv_output = self.energy.pv_present * ...
                self.config.percent_pv_load_factor / 100.0 * ...
                365 * 24;       
        end
        
        function updateConsumption(self)
            %% Quantities to update by Domain
            % Living
            %   - household waste
            %   - food consumed
            %   - food waste
            % MOBILITY
            %   - Fuel Consumption
            %   - Emissions
            % ENERGY
            %   - Domestic Consumption
            %   - Domestic Emissions
%             numberOfAgents = self.numberOfAgents;
%             offspring = self.copy();
            %% FUEL CONSUMPTION
            % CARS
            % (assume km travelled by car is equally distributed between 
            % all household cars)
            % number of cars of a specific type
            num_petrol = sum(self.mobility.fuel == 1, 1);         % petrol cars
            num_diesel = sum(self.mobility.fuel == 2, 1);         % diesel cars
            num_electric = sum(self.mobility.fuel == 3, 1);       % electric cars
            % average efficiency per car of a specific type
            avg_petrol_efficiency = sum(self.mobility.efficiency .* (self.mobility.fuel == 1), 1) ./ num_petrol;
            avg_diesel_efficiency = sum(self.mobility.efficiency .* (self.mobility.fuel == 2), 1) ./ num_diesel;
            % remove NaN/Inf for agents with no cars
            avg_petrol_efficiency(~isfinite(avg_petrol_efficiency)) = 0;
            avg_diesel_efficiency(~isfinite(avg_diesel_efficiency)) = 0;
            % km travelled per car
            km_per_car = self.mobility.km_travelled(3,:) ./ self.mobility.cars;
            % fix km_per_car for households with no cars
            km_per_car(~isfinite(km_per_car)) = 0;
            % consumption of petrol cars
            petrol_consumption = num_petrol .* km_per_car * ...
                self.config.car_consump(1) .* avg_petrol_efficiency;
            diesel_consumption = num_diesel .* km_per_car * ...
                self.config.car_consump(2) .* avg_diesel_efficiency;
            % hybrid car
            electric_consumption = num_electric .* km_per_car * ...
                self.config.car_consump(3) * 0.66;
            petrol_consumption = petrol_consumption + num_electric .* ...
                km_per_car * self.config.car_consump(1) * 0.34;
            % PUBLIC TRANSPORT
            % bus consumption - diesel
            bus_consumption = self.mobility.km_travelled(4,:) * self.config.bus_consump;
            % train consumption - diesel
            train_consumption = self.mobility.km_travelled(5,:) * self.config.train_consump;
            % update offspring fuel consumption
            self.mobility.fuel_consumption = [
                petrol_consumption;
                diesel_consumption + bus_consumption + train_consumption;
                electric_consumption];
            %% EMISSIONS from travel by car, bus, train
            % emissions from petrol/diesel car = km travelled in 
            % petrol/diesel car * petrol/diesel emissions per km
            % emissions from hybrid/electric car = km travelled using
            % petrol engine * emissions per km for petrol car * 0.34
            petrol_emissions = (num_petrol .* km_per_car * ...
                self.config.CO2_emissions(1) + num_electric .* km_per_car * ...
                self.config.CO2_emissions(1) * 0.34) .* avg_petrol_efficiency;
            diesel_emissions = num_diesel .* km_per_car * ...
                self.config.CO2_emissions(2) .* avg_diesel_efficiency;
            % bus emissions
            bus_emissions = self.mobility.km_travelled(4,:) * self.config.CO2_emissions(3);
            % train emissions
            train_emissions = self.mobility.km_travelled(5,:) * self.config.CO2_emissions(4);
            % update offspring emissions
            self.mobility.emissions = petrol_emissions + ...
                diesel_emissions + bus_emissions + train_emissions;
            %% DOMESTIC ENERGY CONSUMPTION
            % heating energy
            other_e = self.getNonHeatingEnergy();
            
            heating_energy = self.energy.annual_base_heat_loss_pu .* ...
                self.getNormalisedBuildingSurfaceArea() .* ...
                self.living.construction - ...
                other_e.contribution_to_heating;
            
            % update domestic energy usage
            self.energy.energy_usage = heating_energy + ...
                other_e.hot_water + ...
                other_e.lightning + ...
                other_e.appliances + ...
                other_e.cooking + ...
                other_e.car_charging;
    
            %% DOMESTIC EMISSIONS
            % first check for PV and output
            pv_output = self.getAnnualPVOutput();           

            % check for OTHER local energy source and determine output if present
            local_output = self.energy.local_energy_source_present(1,:) .* ...
                self.energy.local_energy_source_present(3,:);
            
            % check different heating scenarios
            local_energy_use = min(heating_energy, local_output);
            grid_electricity = max(0, self.energy.energy_usage - ...
                local_energy_use - pv_output);
            
            %% Agents that have a local energy source
            de1 = (local_energy_use .* ...
                   self.energy.local_energy_source_present(4,:) + ...
                   grid_electricity * self.config.CO2_per_kWh_electricity .* ...
                   self.energy.grid_reduction_factor) .* ...
                   ... % conditionals
                   self.energy.local_energy_source_present(1,:) .* ...  % house has local energy source
                   (self.energy.energy_source == 5);                    % local source is used for heating
            
            grid_electricity = max(0, self.energy.energy_usage - ...
                local_output - pv_output);   
            de2 = (grid_electricity * self.config.CO2_per_kWh_electricity .* ...
                   self.energy.grid_reduction_factor + ...
                   local_output .* self.energy.local_energy_source_present(4,:)) .* ...
                   ... % conditionals
                   self.energy.local_energy_source_present(1,:) .* ...  % house has local energy source
                   ~(self.energy.energy_source == 5);                    % local source is only used for electricity 
            %% Agents that don't have a local energy source
            % house has electricity as heating source
            grid_electricity = max(0, self.energy.energy_usage - pv_output);  
            de3 = grid_electricity * ...
                  self.config.CO2_per_kWh_electricity .* ...
                  self.energy.grid_reduction_factor .* ...
                  ~self.energy.local_energy_source_present(1,:) .* ... % no local en src
                  (self.energy.energy_source == 0);                    % uses electricity for heating
            
            % house has gas (assume gas for cooking is negligible)
            gas_energy_use = heating_energy + other_e.hot_water;
            grid_electricity = max(0, self.energy.energy_usage - gas_energy_use - pv_output);
            de4 = (grid_electricity * ...
                   self.config.CO2_per_kWh_electricity .* ...
                   self.energy.grid_reduction_factor + ...
                   gas_energy_use * self.config.CO2_per_kWh_gas) .* ...
                  ~self.energy.local_energy_source_present(1,:) .* ... % no local en src
                  (self.energy.energy_source == 1);                    % uses gas
              
            % house uses oil for heating
            oil_energy_use = heating_energy;
            grid_electricity = max(0, self.energy.energy_usage - oil_energy_use - pv_output);
            de5 = (grid_electricity * ...
                   self.config.CO2_per_kWh_electricity .* ...
                   self.energy.grid_reduction_factor + ...
                   oil_energy_use * self.config.CO2_per_kWh_oil) .* ...
                  ~self.energy.local_energy_source_present(1,:) .* ... % no local en src
                  (self.energy.energy_source == 2);                    % uses oil
              
            % house uses solid energy for heating
            solid_energy_use = heating_energy;
            grid_electricity = max(0, self.energy.energy_usage - solid_energy_use - pv_output);
            de6 = (grid_electricity * ...
                   self.config.CO2_per_kWh_electricity .* ...
                   self.energy.grid_reduction_factor + ...
                   solid_energy_use * self.config.CO2_per_kWh_solid) .* ...
                  ~self.energy.local_energy_source_present(1,:) .* ... % no local en src
                  (self.energy.energy_source == 3);                    % uses solid energy
            
            % TODO: move to tests
%             tmp = logical(de1) + logical(de2) + logical(de3) + logical(de4) + logical(de5) + logical(de6);
%             tabulate(tmp); % should give all ones
            self.energy.domestic_emissions = de1 + de2 + de3 + de4 + de5 + de6;
        end
        
        function adoptInnovation(self, indices, innovation)
            % Adopt innovation. Actual adoptions happens in appropriate
            % adopt*Innovation() method according to selected innovation
            % domain.
            %   self - population
            %   idx - indices of agents that should adopt innovation
            %   innovation - a structure describing innovations
            
            % only adopt if there are potential innovation.impacts
            if any(innovation.impacts) == 1 || isempty(innovation.impacts) == 1
                % only adopt if status is ACTIVE
                adopters = (self.status == 1) & indices;
                if strcmpi(innovation.domain, 'living')
                    self.adoptLivingInnovation(adopters, innovation);
                elseif strcmpi(innovation.domain, 'food')
                    self.adoptFoodInnovation(adopters, innovation);
                elseif strcmpi(innovation.domain, 'mobility')
                    self.adoptMobilityInnovation(adopters, innovation);
                elseif strcmpi(innovation.domain, 'energy')
                    self.adoptEnergyInnovation(adopters, innovation);
                end
            end
        end
        
        function adoptLivingInnovation(self, adopters, innovation)
            % Adopt living innovation
            %   self - population
            %   idx - indices of agents that shoulda dopt innovation
            %   innovation - a structure describing innovations
            if strcmpi( innovation.primary_impact, 'change type' )
                % check if already a cooperative member, if not then adopt
                individual_adopters = (self.living.type ~= 1) & adopters;
                % implement primary impact
                self.living.type(individual_adopters) = 1;
                %% implement secondary Impacts
                % improve construction
                self.living.construction(individual_adopters) = ...
                    self.living.construction(individual_adopters) * ...
                    (1 - innovation.impacts(1) / 100);
                % reduce space
                self.living.floor_area(individual_adopters) = ...
                    self.living.floor_area(individual_adopters) * ...
                    (1 - innovation.impacts(2) / 100);
                self.living.space(individual_adopters) = ...
                    self.floorArea2Space(individual_adopters);
                % reduce energy cooking needs
                self.food.preparation(individual_adopters) = ...
                    self.food.preparation(individual_adopters) * ...
                    (1 - innovation.impacts(3) / 100);
                
                % give access to pv supply if available
                if innovation.impacts(4) == 1 
                    self.energy.pv_present(individual_adopters) = innovation.impacts(5);
                end
                
                % reduce car travel and bump bus and train trevel for
                % households with cars
                ind_adopters_w_cars = self.mobility.cars(individual_adopters) ~= 0;
                delta_km_travelled =  innovation.impacts(6) / 100 * ...
                    self.mobility.km_travelled(3, ind_adopters_w_cars);
                self.mobility.km_travelled(3, ind_adopters_w_cars) = ...
                    self.mobility.km_travelled(3, ind_adopters_w_cars) - ...
                    delta_km_travelled;
                self.mobility.km_travelled(4, ind_adopters_w_cars) = ...
                    self.mobility.km_travelled(4, ind_adopters_w_cars) + ...
                    0.5 * delta_km_travelled;
                self.mobility.km_travelled(5, ind_adopters_w_cars) = ...
                    self.mobility.km_travelled(5, ind_adopters_w_cars) + ...
                    0.5 * delta_km_travelled;
                % reduce km travelled by car to buy food
                self.mobility.km_travelled(3, ind_adopters_w_cars) = ...
                    self.mobility.km_travelled(3, ind_adopters_w_cars) * ...
                    (1 - innovation.impacts(7) / 100);
                % reduce AVOIDABLE food waste
                delta_food_waste = self.food.food_waste(1, individual_adopters) * ...
                    innovation.impacts(8) / 100;
                self.food.food_waste(1, individual_adopters) = ...
                    self.food.food_waste(1, individual_adopters) - ...
                    delta_food_waste;
                % reduction in food waste also reduces household RECYCLABLE
                % waste
                self.living.waste.recyclable(individual_adopters) = ...
                    self.living.waste.recyclable(individual_adopters) - ...
                    delta_food_waste;
                % reduce household waste (recycable and non-recyclable)
                self.living.waste.recyclable(individual_adopters) = ...
                    self.living.waste.recyclable(individual_adopters) * ...
                    (1 - innovation.impacts(9) / 100);
                self.living.waste.nonrecyclable(individual_adopters) = ...
                    self.living.waste.nonrecyclable(individual_adopters) * ...
                    (1 - innovation.impacts(9) / 100);
                
                % set agent as adopter
                self.adopter(individual_adopters) = 1; 
            elseif strcmpi(innovation.primary_impact, 'improve construction')
                self.living.construction(adopters) = ...
                    self.living.construction(adopters) * ...
                    (1 - innovation.impacts(1) / 100);
                % set agent as adopter
                self.adopter(adopters) = 1; 
            elseif strcmpi(innovation.primary_impact, 'reduce household waste')
                self.living.waste.recyclable(adopters) = ...
                    self.living.waste.recyclable(adopters) * ...
                    (1 + innovation.impacts(1) / 100);
                self.living.waste.nonrecyclable(adopters) = ...
                    self.living.waste.nonrecyclable(adopters) * ...
                    (1 - innovation.impacts(2) / 100);
                % set agent as adopter
                self.adopter(adopters) = 1;
            elseif strcmpi( innovation.primary_impact, 'reduce space' )
                % implement primary impact
                self.living.floor_area(adopters) = ...
                    self.living.floor_area(adopters) * ...
                    (1 - innovation.impacts(1) / 100);
                self.living.space(adopters) = self.floorArea2Space(adopters);
                % implement secondary Impacts
                % secondary innovation.impacts (lower energy, lower 
                % emissions) are already built into the model when 
                % consumption is calculated.
                
                % set agent as adopter
                self.adopter(adopters) = 1;
            end
        end
        
        function adoptFoodInnovation(self, idx, innovation)
            % Adopt food innovation
            %   self - population
            %   idx - indices of agents that should adopt innovation
            %   innovation - a structure describing innovations
            if strcmpi(innovation.primary_impact, 'reduce consumption')
                %% implement primary impact
                delta_food_consumed = self.food.food_consumed(idx) * ...
                    (innovation.impacts(1) / 100);
                self.food.food_consumed(idx) = self.food.food_consumed(idx) - ...
                    delta_food_consumed;
                %% implement secondary Impacts
                delta_food_waste = delta_food_consumed * ...
                    self.config.percent_food_waste / 100;
                new_food_waste = self.food.food_consumed(idx) * ...
                    self.config.percent_food_waste / 100;
                self.food.food_waste(1, idx) = new_food_waste * ...
                    self.config.avoidable_waste_percent / 100;
                self.food.food_waste(2, idx) = new_food_waste * ...
                    (1 - self.config.avoidable_waste_percent / 100);
                % reduce RECYCLABLE household waste as a result of reduced
                % food waste
                self.living.waste.recyclable(idx) = ...
                    self.living.waste.recyclable(idx) - delta_food_waste;
                %% set agent as adopter
                self.adopter(idx) = 1;
            elseif strcmpi(innovation.primary_impact, 'reduce waste')
                %% implement primary impact
                % reduce AVOIDABLE food waste
                delta_food_waste = self.food.food_waste(1, idx) * ...
                    innovation.impacts(1) / 100;
                self.food.food_waste(1, idx) = self.food.food_waste(1, idx) - ...
                    delta_food_waste;
                
                % implement secondary Impacts
                self.living.waste.recyclable(idx) = ...
                    self.living.waste.recyclable(idx) - delta_food_waste;
                %% set agent as adopter
                self.adopter(idx) = 1;
            elseif strcmpi(innovation.primary_impact, 'preparation')
                %% implement primary impact
                % reduce cooking energy needs
                self.food.preparation(idx) = self.food.preparation(idx) * ...
                    (1 - innovation.impacts(1) / 100);
                %% implement secondary Impacts
                % secondary innovation.impacts (lower energy, lower 
                % emissions) are already built into the model when 
                % consumption is calculated
                %% set agent as adopter
                self.adopter(idx) = 1;
            elseif strcmpi(innovation.primary_impact, 'change source')
                %% implement primary impact
                % indices of adopters with cars
                w_cars_idx = idx & (self.mobility.cars ~= 0);
                % reduce km travelled for food by CAR
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) * ...
                    (1 - innovation.impacts(1) / 100);
                % reduce household waste
                self.living.waste.recyclable(idx) = ...
                    self.living.waste.recyclable(idx) - innovation.impacts(2);
                self.living.waste.nonrecyclable(idx) = ...
                    self.living.waste.nonrecyclable(idx) - innovation.impacts(3);
                % reduce food waste
                self.food.food_waste(1, idx) = self.food.food_waste(1, idx) - ...
                    innovation.impacts(4);
                self.food.food_waste(2, idx) = self.food.food_waste(2, idx) - ...
                    innovation.impacts(5);
                %% implement secondary Impacts
                delta_food_waste = innovation.impacts(4) + innovation.impacts(5);
                self.living.waste.recyclable(idx) = ...
                    self.living.waste.recyclable(idx) - delta_food_waste;
                % secondary innovation.impacts (lower fuel consumption, 
                % lower emissions) are already built into the model when 
                % consumption is calculated
                %% set agent as adopter
                self.adopter(idx) = 1;
            end
        end
        
        function adoptMobilityInnovation(self, idx, innovation)
            % Adopt mobility innovation
            %   self - population
            %   idx - indices of agents that should adopt innovation
            %   innovation - a structure describing innovations
            if strcmpi(innovation.primary_impact, 'reduce km')  
                %% implement primary impact
                if any(innovation.impacts([1,2,4,5]) > 0) == 1
                    for i = [1, 2, 4, 5]
                        self.mobility.km_travelled(i, idx) = ...
                            self.mobility.km_travelled(i, idx) * ...
                            (1 - innovation.impacts(i) / 100);
                    end
                    %% set agent as adopter
                    self.adopter(idx) = 1;
                end
                % adopters with cars
                w_cars_idx = (self.mobility.cars ~= 0) & idx;
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) * ...
                    (1 - innovation.impacts(3) / 100);
                %% set agent as adopter
                self.adopter(w_cars_idx) = 1;
                %% implement secondary Impacts
                % secondary innovation.impacts (lower fuel consumption,
                % lower emissions) are already built into the model when 
                % consumption is calculated
            elseif strcmpi(innovation.primary_impact, 'change fuel')                      
                %% implement primary impact
                carIndices = self.allCars([1,2], idx);
                % change fuel to electric
                self.mobility.fuel(carIndices) = 3;

                %% set agent as adopter
                self.adopter(sum(carIndices, 1) > 0) = 1;
                %% implement secondary impacts
                % secondary innovation.impacts (fuel consumption, emissions,
                % domestic energy usage) are already built into the model 
                % when consumption is calculated
                
                %-------------------------------------------------
                % INNOVATION NOT WORKING!!!!!!
                %-------------------------------------------------
            elseif strcmpi(innovation.primary_impact, 'increase efficiency')
                %% implement primary impact
                carIndices = self.allCars([1,2], idx);
                self.mobility.efficiency(carIndices) = ...
                     self.mobility.efficiency(carIndices) * ...
                     (1 - innovation.impacts(1) / 100);

                %% set agent as adopter
                self.adopter(sum(carIndices, 1) > 0) = 1;
                %% implement secondary Impacts
                % secondary innovation.impacts (lower fuel consumption, 
                % lower emissions) are already built into the model when 
                % consumption is calculated
            elseif strcmpi(innovation.primary_impact, 'change mode')
                %% implement primary impact
                if any(innovation.impacts(5:end) > 0) == 1
                    bus_to_walking_km = self.mobility.km_travelled(4, idx) * ...
                        innovation.impacts(5) / 100;
                    % reduce bus travel
                    self.mobility.km_travelled(4, idx) = ...
                        self.mobility.km_travelled(4, idx) - bus_to_walking_km;
                    % increase walking
                    self.mobility.km_travelled(1, idx) = ...
                        self.mobility.km_travelled(1, idx) + bus_to_walking_km;
                    
                    bus_to_cycling_km = self.mobility.km_travelled(4, idx) * ...
                        innovation.impacts(6) / 100;
                    % reduce bus travel
                    self.mobility.km_travelled(4, idx) = ...
                        self.mobility.km_travelled(4, idx) - bus_to_cycling_km;
                    % increase cycling
                    self.mobility.km_travelled(2, idx) = ...
                        self.mobility.km_travelled(2, idx) + bus_to_cycling_km;
                    
                    train_to_walking_km = self.mobility.km_travelled(5, idx) * ...
                        innovation.impacts(7) / 100;
                    % reduce train travel
                    self.mobility.km_travelled(5, idx) = ...
                        self.mobility.km_travelled(5, idx) - train_to_walking_km;
                    % increase walking
                    self.mobility.km_travelled(1, idx) = ...
                        self.mobility.km_travelled(1, idx) + train_to_walking_km;
                    
                    train_to_cycling_km = self.mobility.km_travelled(5, idx) * ...
                        innovation.impacts(8) / 100;
                    % reduce train travel
                    self.mobility.km_travelled(5, idx) = ...
                        self.mobility.km_travelled(5, idx) - train_to_cycling_km;
                    % increase cycling
                    self.mobility.km_travelled(2, idx) = ...
                        self.mobility.km_travelled(2, idx) + train_to_cycling_km;
                    
                    % set agent as adopter
                    self.adopter(idx) = 1;
                end
                % Indices of adopters with cars
                w_cars_idx = (self.mobility.cars ~= 0) & idx;
                
                car_to_walking_km = self.mobility.km_travelled(3, w_cars_idx) * ...
                    innovation.impacts(1) / 100;
                % reduce car travel
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) - car_to_walking_km;
                % increase walking
                self.mobility.km_travelled(1, w_cars_idx) = ...
                    self.mobility.km_travelled(1, w_cars_idx) + car_to_walking_km;
                
                car_to_cycling_km = self.mobility.km_travelled(3, w_cars_idx) * ...
                    innovation.impacts(2) / 100;
                % reduce car travel
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) - car_to_cycling_km;
                % increase cycling
                self.mobility.km_travelled(2, w_cars_idx) = ...
                    self.mobility.km_travelled(2, w_cars_idx) + car_to_cycling_km;
                
                car_to_bus_km = self.mobility.km_travelled(3, w_cars_idx) * ...
                    innovation.impacts(3) / 100;
                % reduce car travel
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) - car_to_bus_km;
                % increase bus travel
                self.mobility.km_travelled(4, w_cars_idx) = ...
                    self.mobility.km_travelled(4, w_cars_idx) + car_to_bus_km;
                
                car_to_train_km = self.mobility.km_travelled(3, w_cars_idx) * ...
                    innovation.impacts(4) / 100;
                % reduce car travel
                self.mobility.km_travelled(3, w_cars_idx) = ...
                    self.mobility.km_travelled(3, w_cars_idx) - car_to_train_km;
                % increase bus travel
                self.mobility.km_travelled(5, w_cars_idx) = ...
                    self.mobility.km_travelled(5, w_cars_idx) + car_to_train_km;
                
                % set agent as adopter
                self.adopter(w_cars_idx) = 1;
                %% implement secondary Impacts
                % secondary innovation.impacts (fuel consumption, lower 
                % emissions) are already built into the model when 
                % consumption is calculated
            else
                error('Invalid primary impact: %s', innovation.primary_impact);
            end
        end
        
        function adoptEnergyInnovation(self, adopters, innovation)
            % Adopt energy innovation
            %   self - population
            %   idx - indices of agents that shoulda dopt innovation
            %   innovation - a structure describing innovations
            if strcmpi(innovation.primary_impact, 'reduce consumption')
                self.energy.energy_reduction_factors(:, adopters) = ...
                    bsxfun(@times, self.energy.energy_reduction_factors(:, adopters), ...
                    (1 - innovation.impacts / 100)');
                % set agent as adopter
                self.adopter(adopters) = 1;
            elseif strcmpi(innovation.primary_impact, 'change source')
                % adopt PV if selected
                 if innovation.impacts(1) == 1 % install pv
                    adopters_wo_pv_idx = (self.energy.pv_present == 0) & adopters;
                    self.energy.pv_present(adopters_wo_pv_idx) = innovation.impacts(2);
                    % set agent as adopter
                    self.adopter(adopters_wo_pv_idx) = 1;
                 end
                 % adopt OTHER local energy source
                 if innovation.impacts(3) == 1
                     lesp1 = self.energy.local_energy_source_present(1,:);
                     lesp1(adopters) = 1;
                     self.energy.local_energy_source_present(1,:) = lesp1;
                     
                     if innovation.impacts(6) == 1
                         % new local source is used for heating
                         self.energy.energy_source(adopters) = 5;
                         lesp2 = self.energy.local_energy_source_present(2,:);
                         lesp2(adopters) = 1;
                         self.energy.local_energy_source_present(2,:) = lesp2;
                         self.energy.local_energy_source_present(2) = 1;
                     end
                     % fix energy output from energy source
                     lesp3 = self.energy.local_energy_source_present(3,:);
                     lesp3(adopters) = innovation.impacts(4); 
                     self.energy.local_energy_source_present(3,:) = lesp3;
                     % fix emissions from energy source
                     lesp4 = self.energy.local_energy_source_present(4,:);
                     lesp4(adopters) = innovation.impacts(5);
                     self.energy.local_energy_source_present(4,:) = lesp4;
                     % set agent as adopter
                     self.adopter(adopters) = 1;
                 end
            else
                error('Invalid primary impact: %s', innovation.primary_impact);
            end
        end
        
        function spaceVector = floorArea2Space(self, indices)
            % Return updates space index for the agents referred to by
            % indices
            new_floor_area = self.living.floor_area(indices);
            spaceVector = self.living.space(indices);
            spaceVector(new_floor_area < 50) = 1;
            spaceVector(new_floor_area >= 50 & new_floor_area < 50) = 2;
            spaceVector(new_floor_area >= 70 & new_floor_area < 90) = 3;
            spaceVector(new_floor_area >= 90 & new_floor_area <= 110) = 4;
            spaceVector(new_floor_area > 110) = 5;
        end
        
        function linearIndexVector = allCars(self, carType, idx)
            % Select all cars of specific type for each agent in idx.
            % Return a linear index of cars
            %   self - population
            %   carType - array of car types
            %   idx - indices of agents that should be considered
            
            % combine car type and idx indices into index matrix I
            linearIndexVector = bsxfun(@and, ismember(self.mobility.fuel, carType), idx);
        end
        
        function linearIndexVector = randomCar(self, carType, idx)
            % Select a random car of specific type for each agent in idx.
            % Return a linear index of cars
            %   self - population
            %   carType - array of car types
            %   idx - indices of agents that should be considered
            
            % combine car type and idx indices into index matrix I
            I = bsxfun(@and, ismember(self.mobility.fuel, carType), idx);
            % get row indices of random element in each column of I
            [~, R] = max(I .* rand(size(I)));
            % get linear indices of elements with row index R
            L = sub2ind(size(self.mobility.fuel), R, 1:length(R));
            % remove elements from L that do not appear in I
            linearIndexVector = L(any(I));
        end
    end
end