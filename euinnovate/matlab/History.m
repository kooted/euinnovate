classdef History < handle
    %HISTORY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        results = struct();
        keys
        noOfYears
    end
    
    methods
        function self = History(noOfYears, keys)
            self.keys = keys;
            self.noOfYears = noOfYears;
            for i = 1:length(keys)
                self.results.(keys{i}) = zeros(noOfYears + 1, 1);
            end
        end
        
        function setDataPoint(self, key, year, value)
            self.results.(key)(year) = value;
        end
        
        function savePopulationState(self, year, population)
             % count number of adopters and active households            
            self.setDataPoint('adopters', year, ...
                sum(population.adopter == 1) / population.numberOfAgents);
            self.setDataPoint('actives', year, ...
                sum(population.status == 1) / population.numberOfAgents);
            % count household heating sources
            self.setDataPoint('electricity', year, ... 
                sum(population.energy.energy_source == 0) / population.numberOfAgents);
            self.setDataPoint('gas', year, ...
                sum(population.energy.energy_source == 1) / population.numberOfAgents);
            self.setDataPoint('oil', year, ...
                sum(population.energy.energy_source == 2) / population.numberOfAgents);
            self.setDataPoint('solid_fuel', year, ...
                sum(population.energy.energy_source == 3) / population.numberOfAgents);
            % count households with PV installed
            self.setDataPoint('photovoltaics', year, ...
                sum(population.getAnnualPVOutput() > 0) / population.numberOfAgents);            
            % calculate population level energy consuption
            self.setDataPoint('domestic_energy', year, ...
                sum(population.energy.energy_usage) / population.numberOfAgents); 
            % population level waste
            self.setDataPoint('waste', year, ...
                sum(population.living.waste.nonrecyclable) / population.numberOfAgents);
            % food consumed
            self.setDataPoint('food_consumed', year, ...
                sum(population.food.food_consumed) / population.numberOfAgents);
            % food waste
            self.setDataPoint('food_waste', year, ...
                sum(population.food.food_waste(1,:)) / population.numberOfAgents);
            % calculate population level domestic emissions
            self.setDataPoint('domestic_emissions', year, ...
                sum(population.energy.domestic_emissions) / population.numberOfAgents);
            % calculate population level emissions from mobility
            self.setDataPoint('mobility_emissions', year, ...
                sum(population.mobility.emissions) / population.numberOfAgents);
            % count consumption of diesel
            self.setDataPoint('diesel', year, ...
                sum(population.mobility.fuel_consumption(2,:)) / population.numberOfAgents);
            % count consumption of petrol
            self.setDataPoint('petrol', year, ...
                sum(population.mobility.fuel_consumption(1,:)) / population.numberOfAgents);
            % kg consumption
            self.setDataPoint('kg', year, ...
                sum(population.getKgConsumption()) / population.numberOfAgents);
        end
    end
end

