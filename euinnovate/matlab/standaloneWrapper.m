function standaloneWrapper(experimentId, noOfIterations, noOfAgents, configFile, innovationFile, policyFile, dbserver)
% see http://www.bu.edu/tech/support/research/software-and-programming/common-languages/matlab/standalone/
% function myStandalone(n, nprocs)
% Purpose: this optionally serves as the front m-file
%          to your app so your app remain unchanged.
if ischar(experimentId) == 1
    experimentId = str2num(experimentId);
end
if ischar(noOfIterations) == 1
    noOfIterations = str2num(noOfIterations);
end
if ischar(noOfAgents) == 1
    noOfAgents = str2num(noOfAgents);
end

% Call user app, InnovateModel (script or function m-file)
InnovateModel(experimentId, noOfIterations, noOfAgents, configFile, innovationFile, policyFile, true, dbserver);

% Handle the output (s) as intended
if isdeployed     % in standalone mode . . .
    exit;
else
    close all
end

end   % end of function
