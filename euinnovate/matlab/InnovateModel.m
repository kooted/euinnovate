classdef InnovateModel < handle
    %INNOVATEMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        configFile
        innovationFile
        policyFile
        experimentId
        noOfIterations
        noOfAgents
        saveResults
        connection
        config          % Model configuration structure describing a country
        innovation      % Innovation data
        policy          % Policy structure
        dbserver        % db server host name
        result_keys = {'electricity', 'gas', ...
            'oil', 'solid_fuel', 'photovoltaics', 'domestic_energy', ...
            'adopters', 'actives', 'domestic_emissions', 'mobility_emissions', ...
            'diesel', 'petrol', 'waste', 'food_consumed', 'food_waste', 'kg'};
    end
    methods
        function self = InnovateModel(experimentId, noOfIterations, ...
                noOfAgents, configFile, innovationFile, policyFile, saveToDatabase, dbserver)
            % Constructor
            % Default values for input parameters
            if ~exist('noOfIterations', 'var')
                noOfIterations = 10;
            end
            if ~exist('experimentId', 'var')
                experimentId = 1;
            end
            if ~exist('noOfAgents', 'var')
                noOfAgents = 10000;
            end
            if ~exist('configFile', 'var')
                configFile = 'io/scenario.xlsx';
            end
            if ~exist('innovationFile', 'var')
                innovationFile = 'io/innovation.xlsx';
            end
            if ~exist('policyFile', 'var')
                policyFile = 'io/policy.xlsx';
            end
            connect = false;
            if exist('saveToDatabase', 'var') && saveToDatabase == true
                self.saveResults = @self.saveToDatabase;
                connect = true;
              
            else
                self.saveResults = @self.saveToSpreadsheet;
            end
              if ~exist('dbserver', 'var')
                    dbserver = 'localhost';
                end
            self.dbserver = dbserver;
            self.noOfIterations = noOfIterations;
            self.noOfAgents = noOfAgents;
            self.experimentId = experimentId;
            self.configFile = configFile;
            self.innovationFile = innovationFile;
            self.policyFile = policyFile;
            self.setUp(connect);
            self.iterate();
        end
        
        function delete(self)
            % Destructor
            close(self.connection);
        end
        
        function initXLWrite(self)
            %% Initialisation of POI Libs
            % Add Java POI Libs to matlab javapath
            poi_base = '+xlwrite/poi_library/';
            javaaddpath(strcat(poi_base, 'poi-3.8-20120326.jar'));
            javaaddpath(strcat(poi_base, 'poi-ooxml-3.8-20120326.jar'));
            javaaddpath(strcat(poi_base, 'poi-ooxml-schemas-3.8-20120326.jar'));
            javaaddpath(strcat(poi_base, 'xmlbeans-2.3.0.jar'));
            javaaddpath(strcat(poi_base, 'dom4j-1.6.1.jar'));
            javaaddpath(strcat(poi_base, 'stax-api-1.0.1.jar'));
        end
        
        function r = connect(self)
            % Connect to database
            % Add PostgreSQL JDBC driver
            javaaddpath('/usr/share/java/postgresql-jdbc4.jar');
            % Open database connection
            r = database('euinnovate', 'euinnovate', 'meowmeowmeow12', ...
                'Vendor', 'PostgreSQL', 'Server', self.dbserver);
        end
        
        function iterate(self)
            % Execute the model 'noOfIterations' times and write results to
            % database
            for idx = 1 : self.noOfIterations
                disp(strcat('Experiment #', int2str(self.experimentId), ...
                            ' : Iteration #', int2str(idx)));
                results = self.run();
                self.saveResults(idx, results);
            end
        end
        
        function setUp(self, connect)
            % Set up database connection
            if connect == true
                self.connection = self.connect();
            else
                self.initXLWrite();
            end
            % get model configuration parameters
            self.config = xlread.ExcelConfigReader(self.configFile, 1, 'A4:C140').getConfig();
            
            % set extra config parameters
            % rate at which percentage of inert houses decreases as 
            % innovation is adopted by the population
            self.config.Rdecay = -log( self.config.always_inert / self.config.start_inert ); 
            
            % get innovation configuration
            self.innovation = xlread.ExcelInnovationReader(self.innovationFile, 1, 'A4:C140').getInnovation();
            self.policy = xlread.ExcelPolicyReader(self.policyFile, 1, 'B2:AJ2').getPolicy();
        end
        
        function results = run(self)
            % Initialise storage for results
            results = History(self.config.niter, self.result_keys);
            % Execute a single run of the model
            % create initial agents and assigns attributes
            population = Population(self.config, self.noOfAgents);
            population.updateConsumption();
            % Save results for year 1 to results store
            results.savePopulationState(1, population);
            
            % parameters to describe how fast houses change from inert to active
            inert = struct(...
                'perc_inert', self.config.start_inert, ... % initial percentage of inert households
                'adoption_fraction', 0);                   % fraction of adopters
            % Runthe modelthrough years
            for year = 1 : self.config.niter
               inert = self.runYear(year, population, inert, results);
            end
        end
        
        function inert = runYear(self, year, population, inert, results)
            %% INNOVATION ADOPTION
            % Only agents who have not already adopted need to be
            % considered (population.adopter == 0).
            % Adopt with probability self.innovation.prob_adoption
            agent_indices = (population.adopter == 0) & ...
                (rand(1, population.numberOfAgents) < self.policy.adoption_rate(year));
            population.adoptInnovation(agent_indices, self.innovation);
            %% UPDATE CONSUMPTION to include changes from innovation adoption
            population.updateConsumption();
            %% ACTIVATION OF HOUSEHOLDS. Previously INERT households become
            % ACTIVE based on number of new adopters
            % fraction of adopters at this time step
            new_adoption_fraction = sum(population.adopter == 1) / ...
                population.numberOfAgents;
            % calculate percentage of new adopters
            new_perc_inert = self.config.start_inert * ...
                exp(-self.config.Rdecay * new_adoption_fraction);
            % change in fraction of adopters over time step
            delta_frac = new_adoption_fraction - inert.adoption_fraction;
            if delta_frac > 0 
                % fraction of adopters has increased => more households 
                % must now become active
                % calculates the PERCENTAGE of houses that will change 
                % their activity status from INERT to ACTIVE
                delta_inert = inert.perc_inert - new_perc_inert; 
                % calculates the NUMBER of houses that will change their 
                % activity status from INERT to ACTIVE
                delta_houses = round(delta_inert * population.numberOfAgents / 100); 
                % TODO: make sure that the following is needed
                % make sure not to change more houses than exist!
                % indices of inert households
                inert_indices = (population.status == 0);
                if sum(inert_indices) < delta_houses 
                    delta_houses = sum(inert_indices);
                end
                % convert logical index to linear
                inert_linear_indices = find(inert_indices);
                % takes a random sample of inert household indices with size
                % delta_houses
                houses_convert = randsample(inert_linear_indices, delta_houses, false);
                % change status of household from INERT to ACTIVE
                population.status(houses_convert) = 1;
            end
            inert = struct(...
                'perc_inert', new_perc_inert, ...
                'adoption_fraction', new_adoption_fraction);

            % Save results to results store
            results.savePopulationState(year + 1, population);
        end
        
        function saveToSpreadsheet(self, iteration, results)
            %% Data Generation for XLSX
            % Define an xls name
            filename = 'io/results.xlsx';
            sheetName = strcat('Sheet', int2str(iteration));
            headerStartRange = 'A1';
            startRange = 'A2';
            
            time = 0 : self.config.niter;
            keys = results.keys;
            column_names = ['time_step', keys];
            results_array = [time', results.results.(keys{1})];
            for key = keys(2:end)
                results_array = [results_array, results.results.(key{1})];
            end
            
            % Write column headers starting in cell A1
            xlwrite.xlwrite(filename, column_names, sheetName, headerStartRange);
            % Write data, starting in cell A2
            xlwrite.xlwrite(filename, results_array, sheetName, startRange);
        end
        
        function saveToDatabase(self, iteration, results)
            sqlStringTemplate = 'INSERT INTO result (experiment_id, iteration, year, ';
            connHandle = self.connection.Handle;
            stmt = connHandle.createStatement;
            
            % Build column names and results array
            keys = results.keys;
            column_names = keys{1};
            results_array = [results.results.(keys{1})];
            for key = keys(2:end)
                column_names = [column_names, ', ', key{1}];
                results_array = [results_array, results.results.(key{1})];
            end
            sqlStringTemplate = [sqlStringTemplate, column_names, ') VALUES ('];
            % Output results for each year
            for year = 0 : self.config.niter
                sqlValues = [self.experimentId, iteration, year, results_array(year+1,:)];
                sqlValuesStr = [sprintf('%g, ', sqlValues(1:end-1)), ...
                                sprintf('%g', sqlValues(end))];
                sqlString = [sqlStringTemplate, sqlValuesStr, ');'];         
                stmt.addBatch(sqlString);
            end
            stmt.executeBatch;
            stmt.close;
        end
    end
end

