function rv = v_randsample(numberOfResults, weight)
%V_RANDSAMPLE Summary of this function goes here
%   Outputs values from 1 to number of elements in weight array
    [~, ~, samples] = histcounts(rand(numberOfResults,1), ...
                    cumsum([0; weight(:) ./ sum(weight)]));
    rv = samples';
end
