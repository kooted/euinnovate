classdef TestHistory < matlab.unittest.TestCase
    %TESTHISTORY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods (Test)
        function testHistorySimple(tc)
            h = History(5, {'a', 'b'});
            h.setDataPoint('a', 1, 10);
            h.setDataPoint('b', 1, 11);
            h.setDataPoint('a', 4, 40);
            h.setDataPoint('b', 5, 51);
            h.setDataPoint('a', 2, 20);
            
            tc.assertEqual(h.results.('a'), [10, 20, 0, 40, 0, 0]');
            tc.assertEqual(h.results.('b'), [11, 0, 0, 0, 51, 0]');
        end
    end

end
