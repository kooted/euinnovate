# -*- coding: utf-8 -*-
"""Setup the euinnovate application"""
from __future__ import print_function, unicode_literals
import transaction
from euinnovate import model
import os


def bootstrap(command, conf, vars):
    """Place any commands to setup euinnovate here"""

    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    try:
        u = model.User()
        u.display_name = 'ubigene'
        u.email_address = 'eugene.butan@gmail.com'
        u.password = '1234567'
        model.DBSession.add(u)

        g = model.Group()
        g.group_name = 'users'
        g.display_name = 'Users Group'
        g.users.append(u)
        
        model.DBSession.add(g)
        model.DBSession.flush()
        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>

    try:
        configurations = [
            ["Default Scenario", "Scenario", "euinnovate/matlab/io_templates/scenario.xlsx"],
            ["Default Innovation", "Innovation", "euinnovate/matlab/io_templates/innovation.xlsx"],
            ["Default Policy", "Policy", "euinnovate/matlab/io_templates/policy.xlsx"]]
        for name, type_, path in configurations:
            with open(path, "rb") as cfgfile:
                cfg = model.Configuration(name=name, config_type=type_, file=cfgfile.read())
                model.DBSession.add(cfg)
        model.DBSession.flush()
        transaction.commit()
    except:
        print('Warning, there was a problem adding your configuration data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')
