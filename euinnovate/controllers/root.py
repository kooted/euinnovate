# -*- coding: utf-8 -*-
"""Main Controller"""
from euinnovate import model
from euinnovate.controllers.error import ErrorController
from euinnovate.controllers.secure import SecureController
from euinnovate.lib.base import BaseController
from euinnovate.model import DBSession, Configuration, Experiment, Result, User, Group
from euinnovate.mq.tasks import newExperiment
from math import cos, sin, radians
from os import path
from smtplib import SMTP
from tg import app_globals as g
from tg import expose, flash, require, url, lurl, validate
from tg import predicates
from tg import request, response, redirect, tmpl_context
from tg.exceptions import HTTPFound
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tgext.admin.controller import AdminController
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
import base64
import datetime
import json
import time
import traceback
import xlsxwriter
from StringIO import StringIO
import codecs

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for the euinnovate application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    secc = SecureController()
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)

    error = ErrorController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "euinnovate"

    @expose('euinnovate.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('euinnovate.templates.user_guide')
    @require(predicates.not_anonymous())
    def user_guide(self, **kw):
        """Handle the 'user_guide' page."""
        return dict(page='user_guide')

    @expose('euinnovate.templates.results')
    @require(predicates.not_anonymous())
    @expose('json')
    def results(self, ex=None):
        """ Handle the 'results' page.
            :param ex: IDs of selected experiments """
        if isinstance(ex, list):
            ex = [int(e) for e in ex]
        else:
            ex = [int(ex)]
        return dict(
            page='results',
            meow=json.dumps(Result.by_experiment_id(ex)),
            info=Experiment.getExperiments(ex))

    @require(predicates.not_anonymous())
    #@expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    @expose(content_type='application/vnd.ms-excel')
    def download_results(self, ex=[]):
        """ Handle the 'download_results' page.
            :param ex: experiment ID """

        _filename = "EX{}_results_{}.xlsx".format(
            ex,
            datetime.datetime.now().strftime("%Y-%m-%d_%H-%M"))
        response.headers['content-disposition'] = 'attachment; filename={0}'.format(_filename)

        if not isinstance(ex, list):
            ex = [ex]
        # write file to in-memory string
        output = StringIO()
        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        # get experiment results
        results = Result.by_experiment_id(ex)
        # fill in the first column with integers from 0 to 35
        worksheet.write(0, 0, 'year')
        for i in range(0, 36):
            worksheet.write(i + 1, 0, i)
        # fill in the rest columns
        (row, col) = (0, 1)
        for param in results['charts']:
            worksheet.write(row, col, param + " mean") # column with mean values
            row += 1
            # fill out results row by row
            for value in results[param]['mean'][0]: # value is in form (year, value)
                worksheet.write(row, col, value[1])
                row += 1
            row = 0
            col += 1
            worksheet.write(row, col, param + " min") # column with min values
            row += 1
            # fill out results row by row
            for value in results[param]['ci'][0]: # value is in form (min, max)
                worksheet.write(row, col, value[0])
                row += 1
            row = 0
            col += 1
            worksheet.write(row, col, param + " max") # column with max values
            row += 1
            # fill out results row by row
            for value in results[param]['ci'][0]:
                worksheet.write(row, col, value[1])
                row += 1
            row = 0
            col += 1
        workbook.close()
        contents = output.getvalue()
        output.close()
        return contents

    @require(predicates.not_anonymous())
    @expose(content_type='application/vnd.ms-excel')
    def download_config(self, cfg=None, ex=[]):
        """ Handle the 'download_config' page.
            :param cfg: configuration type
            :param ex: experiment ID """
        experiment = DBSession.query(Experiment).filter(Experiment.id == ex).first()
        _filename = "EX{}_{}_{}.xlsx".format(
            experiment.id,
            cfg, 
            datetime.datetime.now().strftime("%Y-%m-%d_%H-%M"))
        response.headers['content-disposition'] = 'attachment; filename={0}'.format(_filename)

        if experiment is None:
            flash("Unable to find the file", error)
        else:
            if cfg == "scenario":
                return experiment.scenario.file
            elif cfg == "innovation":
                return experiment.innovation.file
            elif cfg == "policy":
                return experiment.policy.file
            else:
                flash("Unable to find the file", error)

    @expose('euinnovate.templates.new_experiment')
    @require(predicates.not_anonymous())
    @expose('json')
    def create(self, **kw):
        """ Show "Create new experiment" page
        """
        print Configuration.getInnovations()
        return dict(
            innovations=Configuration.getInnovations(),
            scenarios=Configuration.getScenarios(),
            policies=Configuration.getPolicies(),
            page='create')

    @expose()
    def create_experiment(self, experiment_name, description, scenario, innovation, policy):
        # TODO: add exception handling

        user = request.environ['repoze.who.identity']['user']
        # Experiment.create(experiment_name, description, scenario, innovation, user.user_id)
        if not Experiment.exists(experiment_name):
            r = newExperiment.delay(experiment_name, description, scenario, innovation, policy, user.user_id)
            flash("Successfully created experiment: {}".format(experiment_name))
            redirect("/experiments")
        else:
            flash('The experiment "{}" already exists, please use a different name'.format(experiment_name), 'error')
            redirect("/create")
                
    @expose('euinnovate.templates.experiments')
    @require(predicates.not_anonymous())
    @expose('json')
    def experiments(self, **kw):
        """Handle the 'experiments' page."""
        return dict(experiments=Experiment.list(), page='experiments')

    @expose('euinnovate.templates.upload')
    @require(predicates.not_anonymous())
    def upload(self, **kw):
        """Handle the 'upload' page."""
        return dict(page='upload')

    @require(predicates.not_anonymous())
    @expose('json')
    def new_config(self, cfgtype, cfgfile, cfgname, **kw):
        """ Save uploaded configuration to the database.
            :param cfgtype: Selected configuration type
            :param cfgfile: Configuration file
            :param cfgname: Configuration name input value
        """
        if cfgtype and cfgname and cfgfile.filename:
            if path.splitext(cfgfile.filename)[1] == '.xlsx':
                filecontent = cfgfile.file.read()
                new_cfgfile = Configuration(name=cfgname, config_type=cfgtype, file=filecontent)
                if DBSession.query(Configuration).filter(Configuration.name == cfgname).first() is None:
                    try:
                        DBSession.add(new_cfgfile)
                        DBSession.flush()
                        flash('Configuration file uploaded')
                    except:
                        flash('Unable to save configuration', 'error')
                else:
                    flash('Configuration was not saved. This name is already taken', 'error')
            else:
                flash('Provided file is not in .xlsx format', 'error')
        else:
            flash('Required inputs are missing', 'error')
        redirect("/upload", **kw)

    @expose('euinnovate.templates.login')
    def login(self, came_from=lurl('/'), failure=None, login='', remember=False):
        """Start the user login."""
        if failure is not None:
            if failure == 'user-not-found':
                flash(_('User not found'), 'error')
            elif failure == 'invalid-password':
                flash(_('Invalid Password'), 'error')

        login_counter = request.environ.get('repoze.who.logins', 0)
        if failure is None and login_counter > 0:
            flash(_('Wrong credentials'), 'warning')

        # # if checkbox checked, remember this user
        # if remember:
        #     response.headers = request.environ['repoze.who.plugins']['main_identifier'].remember(request.environ, {'repoze.who.userid':username})

        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from, login=login)

    @expose()
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to 'user_guide' page on successful authentication 
        or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect('/login',
                     params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome, %s!') % userid)

        # Do not use tg.redirect with tg.url as it will add the mountpoint
        # of the application twice.
        #return HTTPFound(location=came_from)
        redirect("/user_guide")

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        return HTTPFound(location=came_from)

    @expose('euinnovate.templates.register')
    def register(self, **kw):
        """
            Handle 'register' page.
        """
        return dict(page='register')

    @expose('json')
    def create_account(self, **kw):
        """
            Create new user account. Passed parameters:
            name
            eml
            pass1
            pass2
        """
        user = User(display_name=kw['name'], password=kw['pass1'], email_address=kw['eml'])
        # check if email already exists in the DB
        if DBSession.query(User).filter(User.email_address == kw['eml']).first() is None:
            try:
                group = DBSession.query(Group).filter(Group.group_name == 'users').first()
                DBSession.add(user)
                group.users.append(user)
                DBSession.flush()
                flash('New user account has been successfully created, you can log in now')
            except:
                flash('Unable to save account to the database', 'error')
        else:
            flash('This email is already registered', 'warning')
        # redirect to login
        redirect("/login")

    @require(predicates.not_anonymous())
    @expose('euinnovate.templates.edit_account')
    def edit_account(self, **kw):
        """
            Handle 'edit_account' page.
        """
        return dict(page='edit_account')

    @require(predicates.not_anonymous())
    @expose('json')
    def save_account_changes(self, **kw):
        """
            Save account details changes. Passed:
            name
            eml
        """
        user = request.environ['repoze.who.identity']['user']
        # check if another account with the same email already exists in the DB
        if kw['eml'] == user.email_address \
            or DBSession.query(User).filter(User.email_address == kw['eml']).first() is None:
            try:
                user.display_name = kw['name']
                user.email_address = kw['eml']
                DBSession.add(user)
                DBSession.flush()
                flash("The account details have been successfully updated")
            except:
                flash('Unable to save account details to the database', 'error')
        else:
            flash('This email is already registered', 'error')
        redirect("/edit_account")

    @require(predicates.not_anonymous())
    @expose('euinnovate.templates.change_password')
    def change_password(self, **kw):
        """
            Handle 'change_password' page.
        """
        return dict(page='change_password')

    @require(predicates.not_anonymous())
    @expose('json')
    def save_new_password(self, **kw):
        """
            Save new password to the DB.
        """
        user = request.environ['repoze.who.identity']['user']
        try:
            user.password = kw['pass1']
            DBSession.add(user)
            DBSession.flush()
            flash("The password has been successfully updated")
        except:
            flash('Unable to save changes to the database', 'error')
        redirect('/change_password')

    @expose('euinnovate.templates.recover_password')
    def recover_password(self, **kw):
        """
            Handle 'recover_password' page.
        """
        return dict(page='recover_password')

    @expose('json')
    def send_password(self, **kw):
        """
            Send password recovery email and redirect to login page
        """
        # format: "eml;now"
        # example: "postbox@gmail.com;20081217"
        eml = kw['eml']
        tmpstr = ';'.join(map(str, (eml, datetime.date.today().strftime("%Y%m%d"))))
        hash = base64.urlsafe_b64encode(g.encryption_key.encrypt(tmpstr + (";"*(16-(len(tmpstr)%16)))))
        recover_url = "http://" + request.headers['Host'] + url("/recover_password_2", params={"rid":hash})

        # if email has display_name, use it for greeting
        greeting = "Hello,"
        user = User.by_email_address(eml)
        if user and user.display_name:
            greeting = "Dear %s," % (user.display_name)
        msg_body = "%s\n\n\nTo renew your EU-INNOVATE password follow the link below:\n\n%s\n\n\nKind regards,\nEU-INNOVATE Model.\n"\
             % (greeting, recover_url)

        subject = "Forgot your EU-INNOVATE password?"
        if send_email("euinnovate.model", "stovzsb&", eml, subject, msg_body):
            flash("Password recovery e-mail sent!")
        else:
            flash("An error encountered while trying to send password recovery e-mail.", "error")
        redirect('/login')

    @expose('euinnovate.templates.reset_password')
    @expose('json')
    def recover_password_2(self, **kw):
        """
            Handle 'reset_password' page.
        """
        rid = kw['rid']
        # TODO: check rid and handle invalid eml or issue date
        strid = g.encryption_key.decrypt(base64.urlsafe_b64decode(str(rid)))
        id = strid.split(';', 2)
        # format: "eml;issue_date"
        user = User.by_email_address(id[0])
        # check if user exists
        if not user:
            flash('Invalid password recovery link, please try again')
            redirect("/login")
        return dict(page='reset_password', params=kw)

    @expose()
    def save_reset_password(self, **kw):
        """
            Save reset password to the DB.
        """
        rid = kw['rid']
        strid = g.encryption_key.decrypt(base64.urlsafe_b64decode(str(rid)))
        id = strid.split(';', 2)
        # format: "eml;issue_date"
        user = User.by_email_address(id[0])
        # check if user exists
        if not user:
            flash('Invalid password recovery link, please try again')
            redirect("/login")
        try:
            user.password = kw['pass1']
            DBSession.add(user)
            DBSession.flush()
            flash("The password has been successfully reset")
        except:
            flash('Unable to save changes to the database', 'error')
            traceback.print_exc()
        redirect('/login')

def send_email(sender, pwd, recipient, subject, body, ssl=False):
    """
        from smtplib import SMTP, SMTP_SSL

        Make sure the following is enabled in sender account:
        https://www.google.com/settings/security/lesssecureapps
        Disable CAPTHA for clients:
        http://www.google.com/accounts/DisplayUnlockCaptcha
    """
    recipient = recipient if type(recipient) is list else [recipient]
    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (sender, ", ".join(recipient), subject, body)
    try:
        if ssl:
            # if you want to use Port 465 you have to create an SMTP_SSL object
            server_ssl = SMTP_SSL("smtp.gmail.com", 465)
            server_ssl.ehlo() # optional, called by login()
            server_ssl.login(sender, pwd)  
            # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 
            server_ssl.sendmail(sender, recipient, message)
            #server_ssl.quit()
            server_ssl.close()
        else:
            server = SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(sender, pwd)
            server.sendmail(sender, recipient, message)
            server.close()
    except:
        print "failed to send mail"
        traceback.print_exc()
        return False
    print 'successfully sent the mail'
    return True

