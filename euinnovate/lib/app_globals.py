# -*- coding: utf-8 -*-

"""The application's Globals object"""

__all__ = ['Globals']

from Crypto.Cipher import AES


class Globals(object):
    """Container for objects available throughout the life of the application.

    One instance of Globals is created during application initialization and
    is available during requests via the 'app_globals' variable.

    """

    def __init__(self): # Do nothing, by default.
        # password recovery hash decoding key
        self.encryption_key = AES.new("1234567890123456")
        pass
