import scipy.stats
from numpy import mean, array, sqrt 


def meanCI(data, confidence=0.95):
    n = len(data)
    m, se = mean(data), scipy.stats.sem(data)
    # calls the inverse CDF of the Student's t distribution
    # print "mult", scipy.stats.t._ppf(confidence, n-1), "mult2", scipy.stats.t._ppf((1+confidence)/2., n-1)
    h = se * scipy.stats.t._ppf((1+confidence)/2., n-1)
    # return m-h, m+h
    return h