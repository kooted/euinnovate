from __future__ import absolute_import

from .celery import app
# from euinnovate import model

# from sqlalchemy.orm import scoped_session
import subprocess
import transaction
from tg import config
from paste.deploy import appconfig

from celery.signals import task_prerun

from os import getcwd
from paste.deploy import loadapp
from webtest import TestApp
from gearbox.commands.setup_app import SetupAppCommand
from tg import config
from tempfile import NamedTemporaryFile

from euinnovate import model

def load_app(name):
    """Load the test application."""
    return TestApp(loadapp('config:standalone.ini#%s' % name, relative_to=getcwd()))

# def setUp():
#     """Setup test fixture for each functional test method."""

tg_app = load_app('main_without_authn')

dburi = config.get('sqlalchemy.url')
dbserver = dburi.split('@')[1].split(':')[0]

@task_prerun.connect
def on_task_init(*args, **kwargs):
    engine = config['tg.app_globals'].sa_engine
    engine.dispose()

@app.task
def newExperiment(name, description, scenario, innovation, policy, owner):
    idx = model.Experiment.create(name, description, scenario, innovation, policy, owner)
    runExperiment.delay(idx)
    return idx
    
@app.task
def runExperiment(idx):
    ex = model.Experiment.by_id(idx)
    scenario_file = innovation_file = ''
    with NamedTemporaryFile(delete=False, suffix='.xlsx') as scenario:
        scenario_file = scenario.name
        scenario.write(ex.scenario.file)
    with NamedTemporaryFile(delete=False, suffix='.xlsx') as innovation:
        innovation_file = innovation.name
        innovation.write(ex.innovation.file)
    with NamedTemporaryFile(delete=False, suffix='.xlsx') as policy:
        policy_file = policy.name
        policy.write(ex.policy.file)
    transaction.commit()
    print str(idx), "100", "10000", scenario_file, innovation_file, policy_file, dbserver
    exit_status = subprocess.call(["bin/run_standalone.sh", str(idx), "100", "10000", scenario_file, innovation_file, policy_file, dbserver])
    print "Execution status:", exit_status
    if exit_status == 0:
        status = u'completed'
    else:
        status = u'failure'
    print "Experiment:", ex
    updateExperimentStatus.delay(idx, status)

@app.task
def updateExperimentStatus(idx, status):
    ex = model.Experiment.by_id(idx)
    ex.status = status
    transaction.commit()