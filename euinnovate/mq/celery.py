from __future__ import absolute_import

from celery import Celery

app = Celery('mq',
             broker='amqp://guest@localhost//',
             backend='rpc://',
             include=['euinnovate.mq.tasks'])

if __name__ == '__main__':
    app.start()
