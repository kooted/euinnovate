from collections import namedtuple
from sqlalchemy import Column
from sqlalchemy.types import Enum


def enum_col(name, enums, **kwargs):
    # default = kwargs.pop("default", enums[0])
    nullable = kwargs.pop("nullable", False)

    vals = namedtuple("{}_values".format(name), enums)(*enums)
    col = Column(Enum(*enums, name=name), nullable=nullable, **kwargs)
    return (col, vals)