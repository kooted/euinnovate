# -*- coding: utf-8 -*-
"""
Auth* related model.

This is where the models used by the authentication stack are defined.

It's perfectly fine to re-use this definition in the euinnovate application,
though.

"""
from datetime import datetime
from euinnovate.lib.util import meanCI
from euinnovate.model import DeclarativeBase, metadata, DBSession, User
from sqlalchemy import Table, ForeignKey, Column
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import exists
from sqlalchemy.types import Unicode, Integer, DateTime, Enum, LargeBinary, Float
from types import enum_col
import numpy
import os
import transaction


__all__ = ['Experiment', 'Configuration', 'Result']

class Experiment(DeclarativeBase):
    """
    Experiment definition

    ``tg_user.user_id`` referenced by ForeignKey in column ``owner``
    """

    __tablename__ = 'experiment'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(100), unique=True, nullable=False)
    description = Column(Unicode(255), nullable=False)
    owner_id = Column(Integer, ForeignKey('tg_user.user_id', 
                                        onupdate="CASCADE", 
                                        ondelete="CASCADE"))
    owner = relationship("User", backref=backref('experiments', 
        order_by="Experiment.id")) 
    created = Column(DateTime, default=datetime.now)
    
    scenario_id = Column(
        Integer, 
        ForeignKey('configuration.id', 
        onupdate="CASCADE", 
        ondelete="CASCADE"))
    scenario = relationship(
        "Configuration",
        foreign_keys=[scenario_id])

    innovation_id = Column(
        Integer, 
        ForeignKey('configuration.id', 
        onupdate="CASCADE", 
        ondelete="CASCADE"))
    innovation = relationship(
        "Configuration",
        foreign_keys=[innovation_id])
    
    policy_id = Column(
        Integer, 
        ForeignKey('configuration.id', 
        onupdate="CASCADE", 
        ondelete="CASCADE"))  

    policy = relationship(
        "Configuration",
        foreign_keys=[policy_id])

    # {'running', 'completed'}
    status = Column('status', Unicode(20))
  
    @classmethod
    def by_id(cls, idx):
        return DBSession.query(cls).filter(cls.id==idx).one()

    @classmethod
    def exists(cls, name):
        return DBSession.query(exists().where(Experiment.name == name)).scalar()

    @classmethod
    def create(cls, name, description, scenario, innovation, policy, owner_id):
        ex = cls(name=name, description=description,
                 owner_id=owner_id, scenario_id=scenario,
                 innovation_id=innovation, policy_id=policy, status='running')
        DBSession.add(ex)
        DBSession.flush()
        rv = ex.id
        transaction.commit()
        return rv

    @classmethod 
    def _list(cls, query):
        rv = []
        for ex in query:
            # XXX: workaround for legacy experiments
            policy = 'None'
            if ex.policy:
                policy = ex.policy.name

            rv.append(dict(
                id=ex.id,
                name=ex.name,
                description=ex.description,
                created=ex.created.strftime('%Y-%m-%d %H:%m:%S'),
                owner=ex.owner.display_name,
                scenario=ex.scenario.name,
                innovation=ex.innovation.name,
                policy=policy,
                status=ex.status))
        return rv

    @classmethod
    def list(cls):
        return cls._list(DBSession.query(cls))

    @classmethod
    def getExperiments(cls, idx):
        return cls._list(DBSession.query(cls).filter(cls.id.in_(idx)))


class Configuration(DeclarativeBase):
    """
    Configuration definition

    Required columns are:
    ``name``
    ``config_type``
    ``file``
    """

    __tablename__ = 'configuration'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(100), unique=True, nullable=False)
    (config_type, config_type_values) = enum_col('config_type', ["Scenario", "Innovation", "Policy"])
    file = Column(LargeBinary, nullable=False)

    @classmethod
    def getConfigurations(cls, type):
        return DBSession.query(cls.id, cls.name).\
            filter(cls.config_type == type).order_by(cls.name).all()

    @classmethod
    def getScenarios(cls):
        return cls.getConfigurations("Scenario")

    @classmethod
    def getInnovations(cls):
        return cls.getConfigurations("Innovation")

    @classmethod
    def getPolicies(cls):
      return cls.getConfigurations("Policy")


class Result(DeclarativeBase):
    __tablename__ = 'result'
    __public__ = ['adopters', 'actives', 'domestic_energy', 
                  'domestic_emissions', 'gas', 'oil', 
                  'solid_fuel', 'photovoltaics', 'electricity',
                  'mobility_emissions', 'diesel', 'petrol',
                  'waste', 'food_waste', 'food_consumed', 'kg']
    # __public__ = ['adopters', 'actives']

    id = Column(Integer, autoincrement=True, primary_key=True)
    experiment_id = Column(Integer, ForeignKey('experiment.id', ondelete="CASCADE"), nullable=False)
    experiment = relationship("Experiment", foreign_keys=[experiment_id], backref=backref('results', order_by="Result.id"))
    year = Column(Integer, nullable=False)
    iteration = Column(Integer, nullable=False)

    electricity = Column(Float)
    gas = Column(Float)
    oil = Column(Float)
    solid_fuel = Column(Float)
    photovoltaics = Column(Float)
    domestic_energy = Column(Float)
    adopters = Column(Float)
    actives = Column(Float)
    domestic_emissions = Column(Float)
    mobility_emissions = Column(Float)
    diesel = Column(Float)
    petrol = Column(Float)
    waste = Column(Float)
    food_consumed = Column(Float)
    food_waste = Column(Float)
    kg = Column(Float)

    @classmethod
    def by_experiment_id(cls, indices):
        assert(isinstance(indices, list))
        areaconf = [{'representation': "symmetric"}];
        rv = dict()
        rv['labels'] = []
        rv['charts'] = cls.__public__
        for i, param in enumerate(cls.__public__):
            rv[param] = dict(mean=[], ci=[])
        
        experiments = DBSession.query(Experiment).\
                filter(Experiment.id.in_(indices))

        for ex in experiments:
            rv['labels'].append("#{} - {}".format(ex.id, ex.name[:20]))
            data = numpy.zeros((len(cls.__public__), 100, 36))
            for result in ex.results:
                for i, param in enumerate(cls.__public__):
                    data[i, result.iteration-1, result.year] = getattr(result, param)  
            # get mean and CI values
            for i, param in enumerate(cls.__public__):
                mean = numpy.mean(data[i], axis=0)
                ci = meanCI(data[i])

                rv[param]['mean'].append([(i, m) for i, m in enumerate(mean)])
                rv[param]['ci'].append([(m-ci, m+ci) for m, ci in zip(mean, ci)])
        return rv
