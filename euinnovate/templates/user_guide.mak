<%inherit file="local:templates.master"/>

<%def name="title()">
User Guide to EU-INNOVATE
</%def>

    <div class="row">
      <div class="col-md-12">
        <div class="page-header">
          <h2>User Guide to EU-INNOVATE Model</h2>
        </div>
      	</div>
            <p>The model simulates effects of adopting user innovations in different country and policy contexts. Individual experiments can be created by specifying scenario, innovation and policy files. Custom scenarios, innovations and policies, collectively termed as 'configurations' can be specified by the user. The following sections explain how to use the model, including creating experiments, viewing a list of existing experiments and different ways to explore results of an experiment.</p>
            <p>To navigate through the application please use the bar at the top of the page </p>

            <h3>Create new experiment</h3>
            <p>The <a href="/create">Create New</a> tab allows to create a new experiment by specifying experiment name, description, scenario, innovation and policy. The scenario is the starting location and/or the conditions for the simulation. A default UK scenario is provided, but you can create your own scenarios and upload them using <a href="/upload">Upload Configuration</a> tab. </p>

            <p>The innovation describes a user innovation whose effects are to be assessed. The model supports 14 different innovation types grouped into 4 domains, namely Living, Food, Mobility and Energy. The innovation file contains parameters for all innovations, but only one innovation can be active simultaneously. The active innovation is selected by using two comboboxes at the top of the innovation spreadsheet. First, select innovation domain and then select the innovation within that domain.</p>

            <p>The policy configuration allows to specify an external pressure factor for innovation adoption. The value of this factor is proportional to the amount of incentives and advertisment budget for adoption of the innovation.</p>





            The next box is the innovation, like switching the type of fuel used. Again simply choose one from the drop down options. After all that 
            is completed you can create the experiment, adding it onto the list on the previous page.</p>
            <img src="${tg.url('/img/Create new.png')}" height="150" alt="Graphs"/>


    		<h3>Experiments</h3>
    		<p>The <a href="http://127.0.0.1:8080/experiments">Experiments</a> page shows you all the experiments that have been carried out by other users. The experiments are organised into a table, proceeding 
    		from left to right is its ID, when it was created, who created it, the specific scenario, the innovation, the policy and its status. 
   			To select an experiment to view simply click on it; to select more than one at the same time, hold down the control or shift key. Whilst the experiment is selected, click 
   			on the view results bar at the top of the chart to view the outcome of the experiment, and to compare with any others that you have selected. You will 
 			also have the option to save these results as an excel spreadsheet so that it can be saved into your area.</p>
 			<img src="${tg.url('/img/Excel upload screenshot.png')}" height="300" alt="Excel spreadsheet"/>
 			<img src="${tg.url('/img/Graphs.png')}" height="300" alt="Graphs"/>
    		

    		
    		<h3>Upload configuration</h3>
    		<p> <a href="http://127.0.0.1:8080/upload">Upload configurations</a> is the part of the model that lets you upload your own scenarios, innovations and policies to EU-INNOVATE and simulate them in your own experiments.  
    		Please read the paragraph on the page showing you how to upload your own policies, innovations and scenarios.</p>
    		<img src="${tg.url('/img/New Config.png')}" height="250" alt="Upload configuration"/>