<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    ${self.meta()}
    <title>${self.title()}</title>
    <link rel="stylesheet" type="text/css" media="screen" href="${tg.url('/css/style.css')}" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs-3.3.6/jq-2.2.0,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.11,b-1.1.2,b-colvis-1.1.2,b-html5-1.1.2,b-print-1.1.2,cr-1.3.1,fc-3.2.1,fh-3.1.1,r-2.0.2,sc-1.4.1,se-1.1.2/datatables.min.css"/>
    ${self.head_content()}
</head>
<body class="${self.body_class()}">
    ${self.main_menu()}
  <div class="container">
    ${self.content_wrapper()}
  </div>
    ${self.footer()}
  <script type="text/javascript" src="https://cdn.datatables.net/t/bs-3.3.6/jq-2.2.0,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.11,b-1.1.2,b-colvis-1.1.2,b-html5-1.1.2,b-print-1.1.2,cr-1.3.1,fc-3.2.1,fh-3.1.1,r-2.0.2,sc-1.4.1,se-1.1.2/datatables.min.js"></script>
  ${self.scripts()}
</body>


<%def name="content_wrapper()">
  <%
    flash=tg.flash_obj.render('flash', use_js=False)
  %>
  % if flash:
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
              ${flash | n}
        </div>
      </div>
  % endif
  ${self.body()}
</%def>


<%def name="body_class()"></%def>
<%def name="meta()">
  <meta charset="${response.charset}" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</%def>
<%def name="head_content()"></%def>
<%def name="scripts()"></%def>

<%def name="title()">  </%def>

<%def name="footer()">
  <footer class="footer">
    <p>&copy; EU-INNOVATE ${h.current_year()}. Spotted a bug or have a suggestion? 
    <a href="mailto:eugene.butan@cranfield.ac.uk">Send us an email</a>!</p>
  </footer>
</%def>

<%def name="main_menu()">
  <!-- Navbar -->
  <nav class="navbar navbar-default">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-content">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <a class="navbar-brand" href="${tg.url('/')}"> -->
      <div class="navbar-brand">
        <span>
        <img src="${tg.url('/img/turbogears_logo.png')}" height="20" alt="TurboGears 2"/>
        ${getattr(tmpl_context, 'project_name', 'turbogears2')}</span>
      </div>
      <!-- </a> -->
    </div>

    <div class="collapse navbar-collapse" id="navbar-content">
      <ul class="nav navbar-nav">
        <li class="${('', 'active')[page=='user_guide']}"><a href="${tg.url('/user_guide')}">User Guide</a></li>
        <li class="${('', 'active')[page=='experiments']}"><a href="${tg.url('/experiments')}">Experiments</a></li>
        <li class="${('', 'active')[page=='create']}"><a href="${tg.url('/create')}">Create New</a></li>
        <li class="${('', 'active')[page=='upload']}"><a href="${tg.url('/upload')}">Upload Configuration</a></li>
      </ul>

    % if tg.auth_stack_enabled:
      <ul class="nav navbar-nav navbar-right">
      % if not request.identity:
        <li><a href="${tg.url('/login')}">Login</a></li>
      % else:
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" 
            href="#">${request.environ['repoze.who.identity']['user']}
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="${tg.url('/edit_account')}">Edit account details</a></li> 
            <li><a href="${tg.url('/change_password')}">Change password</a></li>
            <li><a href="${tg.url('/logout_handler')}">Logout</a></li>
          </ul>
        </li>
        <!-- <li><a href="${tg.url('/logout_handler')}">Logout</a></li> -->
        <!-- <li><a href="${tg.url('/admin')}">Admin</a></li> -->
      % endif
      </ul>
    % endif
    </div>
  </nav>
</%def>

</html>
