<%inherit file="local:templates.master"/>

<%def name="title()">Edit account details</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>

<div class="well col-sm-5">
    <form action="send_password" method="post" accept-charset="UTF-8" enctype="multipart/form-data" data-toggle="validator" role="form">
        <h3 class="form-signin-heading">Recover password</h3>

        <div class="form-group">
            <label for="eml" class="sr-only">Email address</label>
            <input type="email" class="form-control" id="eml" name="eml" required />
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Recover</button>
                </div>
            </div>
        </div>

    </form>
</div>