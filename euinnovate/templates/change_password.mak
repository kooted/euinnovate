<%inherit file="local:templates.master"/>

<%def name="title()">Change password</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>

<div class="well col-sm-5">
    <form action="save_new_password" method="post" accept-charset="UTF-8" enctype="multipart/form-data" data-toggle="validator" role="form">
        <h3 class="form-signin-heading">Set new password</h3>

        <div class="form-group">
            <label for="pass1" class="sr-only">New password</label>
            <input type="password" class="form-control" id="pass1" name="pass1" placeholder="New password" data-minlength="6" required />
            <div class="help-block">Minimum of 6 characters</div>
        </div>

        <div class="form-group">
            <label for="pass2" class="sr-only">Confirm password</label>
            <input type="password" class="form-control" id="pass2" name="pass2" placeholder="Confirm password"
                     data-match="#pass1" data-match-error="Password does not match" required />
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>

    </form>
</div>