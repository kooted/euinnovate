<%inherit file="local:templates.master"/>

<%def name="title()">
  Upload configuration
</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>


<div class="well col-sm-5">
<form method="post" action="new_config" enctype="multipart/form-data" data-toggle="validator" role="form">
  <div class="form-group">
    <div class="btn-group btn-group-justified">
      <label for="cfgtype" class="control-label sr-only">Select configuration type</label>
      <select id="cfgtype" name="cfgtype" class="form-control" required>
        <option value="" selected hidden>Select configuration type</option>
        <option>Scenario</option>
        <option>Innovation</option>
        <option>Policy</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <h4>Configuration Template</h4>
    <p class="help-block">Please use the provided MS Excel templates below to create a new configuration for the experiment. The template format depends on the selected configuration option. Modify the template, save and upload it using the form below. Provide a friendly name for your configuration, which will be used later in setting up new experiments. Alternatively, modify and upload a respective configuration from an existing experiment by downloading it from the experiment's description on 'View Results' page.</p>
    <ul>
        <li><a href="${tg.url('/templates/scenario.xlsx')}">Scenario template</a></li>
        <li><a href="${tg.url('/templates/innovation.xlsx')}">Innovation template</a></li>
        <li><a href="${tg.url('/templates/policy.xlsx')}">Policy template</a></li>
    </ul>
  </div>

  <div class="form-group">
    <div class="btn-group btn-group-justified">
        <label class="btn btn-primary" for="cfgfile">
            <input id="cfgfile" name="cfgfile" type="file" style="display:none;" 
                    onchange='$("#uploadFileInfo").html($(this).val());' accept=".xlsx" required />
            Choose File...
        </label>
    </div>
  </div>

  <div class="form-group">
    <span class='label label-info' id="uploadFileInfo"></span>
  </div>

  <div class="form-group">
    <div class="btn-group btn-group-justified">
        <label for="cfgname" class="control-label sr-only">Configuration name</label>
        <input type="text" class="form-control" id="cfgname" name="cfgname" placeholder="Configuration name" required />
    </div>
  </div>

  <div class="form-group">
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <button type="submit" class="btn btn-primary">Upload configuration</button>
        </div>
    </div>
  </div>

</form>
</div>
