<%inherit file="local:templates.master"/>

<%def name="title()">
  Create new experiment
</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>

<form class="form-horizontal" method="post" action="create_experiment" enctype="multipart/form-data" 
      data-toggle="validator" role="form">
  <div class="form-group">
    <label for="experiment_name" class="col-sm-2 control-label">Experiment name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="experiment_name" name="experiment_name" placeholder="An experiment" required />
    </div>
  </div>

  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Experiment description</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="description" name="description" placeholder="Description" required />
    </div>
  </div>

  <div class="form-group">
    <label for="scenario" class="col-sm-2 control-label">Scenario</label>
    <div class="col-sm-5">
      <select id="scenario" name="scenario" class="form-control ">
        %for id, name in scenarios:
          <option value="${id}">${name}</option>
        %endfor
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="innovation" class="col-sm-2 control-label">Innovation</label>
    <div class="col-sm-5">
      <select id="innovation" name="innovation" class="form-control" required>
        %for id, name in innovations:
          <option value="${id}">${name}</option>
        %endfor
      </select>
    </div>
  </div>

    <div class="form-group">
    <label for="policy" class="col-sm-2 control-label">Policy</label>
    <div class="col-sm-5">
      <select id="policy" name="policy" class="form-control" required>
        %for id, name in policies:
          <option value="${id}">${name}</option>
        %endfor
      </select>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-5">
        <div class="btn-group btn-group-justified" required>
            <div class="btn-group">
                <button type="submit" class="btn btn-primary">Create experiment</button>
            </div>
        </div>
    </div>
  </div>

</form>