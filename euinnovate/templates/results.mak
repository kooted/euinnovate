<%inherit file="local:templates.master"/>

<%def name="title()">
    Experiment results
</%def>

<%def name="head_content()">
    <link rel="stylesheet" type="text/css" media="screen" href="${tg.url('/javascript/jquery.jqplot/jquery.jqplot.css')}" />
</%def>

<div class="page-header">
<h1>Experiment results</h1>
</div>

<div class="row">
    %for exp_info in info:
        <div class="col-sm-6">
            <div class="panel panel-default" id="info_panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-list-alt"></span> #${exp_info['id']}: ${exp_info['name']}</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                     
                        <tbody>
                          <tr>
                            <td>Description</td>
                            <td>${exp_info['description']}</td>
                          </tr>
                          <tr>
                            <td>Scenario</td>
                            <td><div class="row">
                                    <div class="col-sm-10">${exp_info['scenario']}</div>
                                    <div class="col-sm-2">
                                        <a type="button" class="btn btn-primary"
                                            href="${tg.url('/download_config')}?cfg=scenario&amp;ex=${exp_info['id']}"
                                            download="scenario_${exp_info['id']}">
                                            <span class="glyphicon glyphicon-save"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Innovation</td>
                            <td><div class="row">
                                    <div class="col-sm-10">${exp_info['innovation']}</div>
                                    <div class="col-sm-2">
                                        <a type="button" class="btn btn-primary"
                                            href="${tg.url('/download_config')}?cfg=innovation&amp;ex=${exp_info['id']}"
                                            download="innovation_${exp_info['id']}">
                                            <span class="glyphicon glyphicon-save"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Policy</td>
                            <td><div class="row">
                                    <div class="col-sm-10">${exp_info['policy']}</div>
                                    <div class="col-sm-2">
                                        <a type="button" class="btn btn-primary ${('', 'disabled')[exp_info['policy']=='None']}"
                                            href="${tg.url('/download_config')}?cfg=policy&amp;ex=${exp_info['id']}"
                                            download="policy_${exp_info['id']}">
                                            <span class="glyphicon glyphicon-save"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                    <a type="button" class="btn btn-primary btn-block" 
                        href="${tg.url('/download_results')}?ex=${exp_info['id']}"
                        download="experiment_${exp_info['id']}">Download results as Excel file</a>
                </div>
            </div>
        </div>
    %endfor
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Adopters</h3>
            </div>
            <div class="panel-body">
                <div id="chart_adopters"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Actives</h3>
            </div>
            <div class="panel-body">
                <div id="chart_actives"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Domestic Energy</h3>
            </div>
            <div class="panel-body">
                <div id="chart_domestic_energy"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Domestic Emissions</h3>
            </div>
            <div class="panel-body">
                <div id="chart_domestic_emissions"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Gas</h3>
            </div>
            <div class="panel-body">
                <div id="chart_gas"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Oil</h3>
            </div>
            <div class="panel-body">
                <div id="chart_oil"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Solid Fuel</h3>
            </div>
            <div class="panel-body">
                <div id="chart_solid_fuel"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Photovoltaics</h3>
            </div>
            <div class="panel-body">
                <div id="chart_photovoltaics"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Electricity</h3>
            </div>
            <div class="panel-body">
                <div id="chart_electricity"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Mobility Emissions</h3>
            </div>
            <div class="panel-body">
                <div id="chart_mobility_emissions"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Diesel</h3>
            </div>
            <div class="panel-body">
                <div id="chart_diesel"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Petrol</h3>
            </div>
            <div class="panel-body">
                <div id="chart_petrol"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Waste</h3>
            </div>
            <div class="panel-body">
                <div id="chart_waste"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Food Waste</h3>
            </div>
            <div class="panel-body">
                <div id="chart_food_waste"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Food Consumed</h3>
            </div>
            <div class="panel-body">
                <div id="chart_food_consumed"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-signal"></span> Kilograms Consumed</h3>
            </div>
            <div class="panel-body">
                <div id="chart_kg"></div>
            </div>
        </div>
    </div>
</div>

<%def name="scripts()">
    <script type="text/javascript" charset="utf-8">
        var meow = $.parseJSON('${meow | n}');
    </script>
    <script src="${tg.url('/javascript/jquery.jqplot/jquery.jqplot.min.js')}"></script>
    <script src="${tg.url('/javascript/jquery.jqplot/jquery.jqplot.min.js')}"></script>
    <script src="${tg.url('/javascript/jquery.jqplot/plugins/jqplot.enhancedLegendRenderer.js')}"></script>
    <script src="${tg.url('/javascript/ui.js')}"></script>
</%def>