<%inherit file="local:templates.master"/>

<%def name="title()">
    Experiment list
</%def>

<%def name="head_content()">

</%def>

<table id="tbExperiments" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Created</th>
            <th>Owner</th>
            <th>Scenario</th>
            <th>Innovation</th>
            <th>Policy</th>
            <th data-sorter="false">Status</th>
        </tr>
    </thead>
    <tbody>
        %for experiment in experiments:
            <tr>
                <td>${experiment['id']}</td>
                <td>${experiment['name']}</td>
                <td>${experiment['created']}</td>
                <td>${experiment['owner']}</td>
                <td>${experiment['scenario']}</td>
                <td>${experiment['innovation']}</td>
                <td>${experiment['policy']}</td>
                <td class="text-center">
                  <!-- Display experiment status as icon -->
                  %if experiment['status'] == 'Completed':
                    <span class="glyphicon glyphicon-ok"></span>
                  %elif experiment['status'] == 'Running':
                    <span class="glyphicon glyphicon-time"></span>
                  %else:
                    <span class="glyphicon glyphicon-warning-sign"></span>
                  %endif
                </td>
            </tr>
        %endfor
    </tbody>
</table>
            

<%def name="scripts()">
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            var table = $('#tbExperiments').DataTable({
                "ajax": {
                    "url": "experiments.json",
                    "dataSrc": "experiments"}, 
                deferRender: true,
                columns: [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'created' },
                    { data: 'owner' },
                    { data: 'scenario' },
                    { data: 'innovation' },
                    { data: 'policy' },
                    { data: 'status' }], 
                rowId: 'id',
                columnDefs: [{
                    "targets": [0],
                    "visible": true,
                    "searchable": false}], 
                buttons: [
                    'selectNone',
                    {
                        extend: 'selected',
                        text: 'View results',
                        action: function(e, dt, button, config) {
                            var selEx = dt.rows({ selected: true }).ids().toArray();
                            window.location.href = "${tg.url('/results')}" + 
                                "?" + $.param({ex: selEx}, true);
                        }}], 
                select: true,
                dom: "<'row'<'col-sm-3'l><'col-sm-5'B><'col-sm-4'f>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            });

            $('#btnViewResults').click( function () {
                window.location.href = "${tg.url('/results')}";
            } );

            setInterval( function () {
                table.ajax.reload(null, false); // user paging is not reset on reload
            }, 30000 );
        });
    </script>
</%def>
