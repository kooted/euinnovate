<%inherit file="local:templates.master"/>

<%def name="title()">Login form</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>

<%def name="main_menu()">
  <div class="container">
    <div class="page-header">
        <h3>
            <a class="text-muted" href="${tg.url('/index')}">EU-INNOVATE Model</a>
        </h3>
    </div>
  </div> <!-- /container -->
</%def>


<div class="well col-sm-5">
    <form class="form-signin" method="post" accept-charset="UTF-8" action="${tg.url('/login_handler', params=dict(came_from=came_from, __logins=login_counter))}" data-toggle="validator" role="form">
        <h3 class="form-signin-heading">Login</h3>

        <div class="form-group">
            <label for="login" class="sr-only">Email address</label>
            <input type="email" class="form-control" id="inputEmail" name="login" value="${login}" placeholder="Email address" required autofocus />
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <label for="password" class="sr-only">Password</label>
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required />
            <div class="help-block with-errors"></div>
        </div>

<!--         <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" value="remember-me"> Remember me
                </label>
            </div>
        </div> -->

        <div class="form-group">
            <div class="btn-group btn-group-justified">
                <div class="btn-group col-sm-2.5">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
                <div class="btn-group col-sm-2.5">
                    <a class="btn btn-default" href="${tg.url('/register')}" role="button">Register</a>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div>
                <a href="${tg.url('/recover_password')}">Recover password</a>
            </div>
        </div>

    </form>
</div>