<%inherit file="local:templates.master"/>

<%def name="title()">Edit account details</%def>

<%def name="scripts()">
  <script src="${tg.url('/javascript/validator.js')}"></script>
</%def>

<div class="well col-sm-5">
    <form action="save_account_changes" method="post" accept-charset="UTF-8" enctype="multipart/form-data" data-toggle="validator" role="form">
        <h3 class="form-signin-heading">Edit account details</h3>

        <div class="form-group">
            <label for="name" class="sr-only">Dispay name</label>
            <input type="text" class="form-control" id="name" name="name" 
                placeholder="${request.environ['repoze.who.identity']['user'].display_name}"
                value="${request.environ['repoze.who.identity']['user'].display_name}" 
                required />
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <label for="eml" class="sr-only">Email address</label>
            <input type="email" class="form-control" id="eml" name="eml" 
                placeholder="${request.environ['repoze.who.identity']['user'].email_address}"
                value="${request.environ['repoze.who.identity']['user'].email_address}"
                required />
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>

    </form>
</div>