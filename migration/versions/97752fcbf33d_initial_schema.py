"""Initial Schema

Revision ID: 97752fcbf33d
Revises: None
Create Date: 2016-08-03 12:49:30.550743

"""

# revision identifiers, used by Alembic.
revision = '97752fcbf33d'
down_revision = None

from alembic import op
from sqlalchemy import Column, Integer, ForeignKey
import sqlalchemy as sa


def upgrade():
    op.add_column('experiment',
        Column(
            'policy_id', Integer(), 
            ForeignKey('configuration.id', 
                       onupdate="CASCADE", 
                       ondelete="CASCADE")))


def downgrade():
    op.drop_column('experiment', 'policy_id')
