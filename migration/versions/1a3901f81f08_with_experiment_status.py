"""With experiment status

Revision ID: 1a3901f81f08
Revises: 4520d3e0d569
Create Date: 2016-10-24 07:38:24.555294

"""

# revision identifiers, used by Alembic.
revision = '1a3901f81f08'
down_revision = '4520d3e0d569'

from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, Unicode, Integer
from euinnovate import model
from euinnovate.model import Experiment, DBSession, Configuration
from paste.deploy import appconfig

from celery.signals import task_prerun

from os import getcwd
from paste.deploy import loadapp
from webtest import TestApp
from gearbox.commands.setup_app import SetupAppCommand
from tg import config
import transaction

from sqlalchemy.sql import table, column
from sqlalchemy.sql.expression import join, select
from sqlalchemy.sql.functions import count
from sqlalchemy import String
from alembic import op





def upgrade():
    conn = op.get_bind()
    op.add_column('experiment', Column('status', Unicode(20)))
    experiment = table('experiment',
        column('status', Unicode(20)),
        column('id', Integer)
    )
    result = table('result',
        column('id', Integer),
        column('experiment_id', Integer)
    )

    j = join(experiment, result,
        experiment.c.id == result.c.experiment_id)
    stmt = select([experiment.c.id, count(result.c.id)]).select_from(j).group_by(experiment.c.id)

    # print SQL
    # print stmt.compile()
    
    for experiment_id, no_of_results in conn.execute(stmt):
        if no_of_results == 3600:
            status = 'completed'
        else:
            status = 'running'

        op.execute(
            experiment.update().\
            where(experiment.c.id==op.inline_literal(1)).\
            values({'status':op.inline_literal(status)})
        )

    # print conn.execute("select * from experiment").fetchall()
    transaction.commit()


def downgrade():
    op.drop_column('experiment', 'status')
