"""with_kg

Revision ID: 4520d3e0d569
Revises: 97752fcbf33d
Create Date: 2016-09-18 11:39:21.216178

"""

# revision identifiers, used by Alembic.
revision = '4520d3e0d569'
down_revision = '97752fcbf33d'

from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, Float


def upgrade():
    op.add_column('result', Column('kg', Float()))


def downgrade():
    op.drop_column('result', 'kg')

